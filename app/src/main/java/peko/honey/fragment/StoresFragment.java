package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import peko.honey.Main2Activity;
import peko.honey.adapter.RestaurentAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.RestaurentEntity;
import peko.honey.onclick.OnclickRestaurent;

/**
 * Created by macshop1 on 12/5/17.
 */

public class StoresFragment extends MyFragment implements ApiConstants,ApiHandle.ApiCallback,OnclickRestaurent,SwipeRefreshLayout.OnRefreshListener{
    public static String TAG = "StoresFragment";
    View root;
    RestaurentEntity entity;
    RestaurentAdapter adapter;
    ArrayList<RestaurentEntity> results = new ArrayList<>();
    RecyclerView rcResList;
    int idShop, exchangeRate, giftPoints, pagePekoShop;
    String nameShop, imgShop, addressShop, location;
    boolean isAlive, isLoading, firstLoading, isRefresh;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager mLayoutManager;
    RelativeLayout relaySearch;
    Main2Activity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.stores_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcResList = (RecyclerView) findViewById(R.id.rcResList);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        relaySearch = (RelativeLayout)findViewById(R.id.relaySearch);
        activity = (Main2Activity)getActivity();
        activity.refreshActionBar(getView());
        isAlive = true;

        swipeRefreshLayout.setOnRefreshListener(this);


        getPekoStore(pagePekoShop);

        relaySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogSearch();
            }
        });
    }


    void reLoadStores() {
        results = new ArrayList<>();
        // adapter = new RestaurentAdapter(getApplicationContext(), results, this);
        // rcResList.setAdapter(adapter);

        firstLoading = false;
        pagePekoShop = 0;
        getPekoStore(pagePekoShop);
    }

    void getPekoStore(int page) {
        ApiHandle.getPekoStore(API_PEKOSTORE, page, this, getActivity());
    }

    void searchPekoShop(String textSearch) {
        ApiHandle.searchShop(API_SEARCH, textSearch, this, getActivity());
    }

    public void showDialogSearch() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.search_shop_dialog);


        final EditText edSearch = (EditText) dialog.findViewById(R.id.edSearch);

        edSearch.requestFocus();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchPekoShop(edSearch.getText().toString());
                    dialog.dismiss();

                    return true;
                }
                return false;
            }
        });

        dialog.show();


    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if(isAlive)
        {

            if (api.equals(API_PEKOSTORE)) {
                if (result != null) {

                    //rcResList.hideShimmerAdapter();
                    pagePekoShop += 1;
                    isLoading = false;
                    if (isRefresh == true) {
                        isRefresh = false;
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    if (firstLoading == false) {

                        firstLoading = true;

                        try {
                            JSONObject jsonObject = new JSONObject(result);

                            JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));

                            for (int i = 0; i < jsonArrayCards.length(); i++) {
                                JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
                                JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
                                JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
                                JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);

                                location = "";


                                if (isDoesString(jsonObjectBranches, "address"))
                                    location = (jsonObjectBranches.getString("address"));


                                if (isDoesString(jsonObjectCards, "id"))
                                    idShop = jsonObjectCards.getInt("id");

                                if (isDoesString(jsonObjectCards, "exchangeRate"))
                                    exchangeRate = jsonObjectCards.getInt("exchangeRate");

                                if (isDoesString(jsonObjectCards, "giftPoints"))
                                    giftPoints = jsonObjectCards.getInt("giftPoints");

                                if (isDoesString(jsonObjectShop, "cover"))
                                    imgShop = jsonObjectShop.getString("cover");

                                if (isDoesString(jsonObjectShop, "name"))
                                    nameShop = jsonObjectShop.getString("name");

                                if (isDoesString(jsonObjectBranches, "address"))
                                    addressShop = jsonObjectBranches.getString("address");

                                entity = new RestaurentEntity(idShop, exchangeRate, giftPoints, imgShop, nameShop, addressShop, jsonArrayBranches.length(), "", location);
                                results.add(entity);
                            }

                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rcResList.setLayoutManager(mLayoutManager);

                            adapter = new RestaurentAdapter(getActivity(), results, this);
                            rcResList.setAdapter(adapter);

                            rcResList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                                @Override
                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                    super.onScrolled(recyclerView, dx, dy);
                                }

                                @Override
                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                    int totalItemCount = mLayoutManager.getItemCount();
                                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                                    if (totalItemCount > 1) {
                                        if (lastVisibleItem >= totalItemCount - 5) {

                                            if (!isLoading)
                                                getPekoStore(pagePekoShop);
                                        }
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {

                            System.out.println("chay den page 1");

                            JSONObject jsonObject = new JSONObject(result);

                            JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));

                            for (int i = 0; i < jsonArrayCards.length(); i++) {
                                JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
                                JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
                                JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
                                JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);
                                location = "";

                                for (int j = 0; j < jsonArrayBranches.length(); j++) {
                                    JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(j);

                                    if (isDoesString(jsonObjectBranches1, "address"))

                                        location += (jsonObjectBranches1.getString("address") + "\n \n");

                                }


                                if (isDoesString(jsonObjectCards, "id"))
                                    idShop = jsonObjectCards.getInt("id");

                                if (isDoesString(jsonObjectCards, "exchangeRate"))
                                    exchangeRate = jsonObjectCards.getInt("exchangeRate");

                                if (isDoesString(jsonObjectCards, "giftPoints"))
                                    giftPoints = jsonObjectCards.getInt("giftPoints");

                                if (isDoesString(jsonObjectShop, "cover"))
                                    imgShop = jsonObjectShop.getString("cover");

                                if (isDoesString(jsonObjectShop, "name"))
                                    nameShop = jsonObjectShop.getString("name");

                                if (isDoesString(jsonObjectBranches, "address"))
                                    addressShop = jsonObjectBranches.getString("address");

                                entity = new RestaurentEntity();
                                entity.setId(idShop);
                                entity.setExchangerate(exchangeRate);
                                entity.setDiamond(giftPoints);
                                entity.setCover(imgShop);
                                entity.setAlllcation(location);
                                entity.setName(nameShop);
                                entity.setLocation(addressShop);
                                entity.setBrand(jsonArrayBranches.length());
                                results.add(entity);
                            }

                            adapter.notifyDataSetChanged();

                            rcResList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                                @Override
                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                    super.onScrolled(recyclerView, dx, dy);
                                }

                                @Override
                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                    int totalItemCount = mLayoutManager.getItemCount();
                                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                                    if (totalItemCount > 1) {
                                        if (lastVisibleItem >= totalItemCount - 5) {

                                            if (!isLoading)
                                                getPekoStore(pagePekoShop);
                                        }
                                    }
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                } else {

                    getPekoStore(pagePekoShop);

                }
            }
            else if (api.equals(API_SEARCH)) {

                if (result != null) {

                    // rcResList.hideShimmerAdapter();
                    pagePekoShop = 0;
                    isLoading = false;
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        JSONObject jsonObjectResult = new JSONObject(jsonObject.getString("result"));

                        JSONArray jsonArrayShops = new JSONArray(jsonObjectResult.getString("shops"));

                        results = new ArrayList<>();
                        adapter = new RestaurentAdapter(getActivity(), results, this);
                        rcResList.setAdapter(adapter);

                        for (int i = 0; i < jsonArrayShops.length(); i++) {
                            JSONObject jsonObjectShop = jsonArrayShops.getJSONObject(i);

                            JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));

                            JSONObject jsonObjectBrand = jsonArrayBranches.getJSONObject(0);

                            if (isDoesString(jsonObjectShop, "id"))
                                idShop = jsonObjectShop.getInt("id");

//                            if (isDoesString(jsonObjectCards, "exchangeRate"))
//                                exchangeRate = jsonObjectCards.getInt("exchangeRate");
//
//                            if (isDoesString(jsonObjectCards, "giftPoints"))
//                                giftPoints = jsonObjectCards.getInt("giftPoints");

                            if (isDoesString(jsonObjectShop, "cover"))
                                imgShop = jsonObjectShop.getString("cover");

                            if (isDoesString(jsonObjectShop, "name"))
                                nameShop = jsonObjectShop.getString("name");

                            if (isDoesString(jsonObjectBrand, "address"))
                                addressShop = jsonObjectBrand.getString("address");

                            entity = new RestaurentEntity(idShop, 0, 0, imgShop, nameShop, addressShop, jsonArrayBranches.length(), "");
                            results.add(entity);

                        }

                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rcResList.setLayoutManager(mLayoutManager);

                        adapter = new RestaurentAdapter(getActivity(), results, this);
                        rcResList.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                }
            }
        }

    }

    @Override
    public void onclickRestaurent(View v, RestaurentEntity entity) {
        sendBoardCastRestaurent(BROADMAIN,DETAILRESTAURENT,entity.getCover(),entity.getName(),entity.getLocation(),entity.getBrand(),entity.getDiamond(),entity.getId(),entity.getExchangerate());
    }

    @Override
    public void onclickExChange(View v, RestaurentEntity entity) {
        sendBoardCastExchageGift(BROADMAIN,EXCHANGEGIFT,entity.getDiamond(),entity.getName(),entity.getExchangerate(),entity.getId());
    }

    @Override
    public void onclickViewLocation(View v, RestaurentEntity entity) {
        String str = "#";
        String str1 = "\n \n";
        showDialogShowLocation(entity.getLocation().replaceAll(str, str1));

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        isRefresh = true;
        reLoadStores();
    }


    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }

}
