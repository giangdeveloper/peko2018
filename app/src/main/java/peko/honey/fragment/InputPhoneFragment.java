package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.LoginActivity;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;

/**
 * Created by macshop1 on 11/20/17.
 */

public class InputPhoneFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {
    View root;
    public static String TAG = "InputPhoneFragment";
    EditText edPhone;
    TextView tvNext;
    LoginActivity activity;
    boolean isAlive;
    String status;
    CatLoadingView mView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.inputphone_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvNext = (TextView) findViewById(R.id.tvNext);
        edPhone = (EditText) findViewById(R.id.edPhone);
        activity = (LoginActivity) getActivity();
        activity.refreshActionBar(getView());

        isAlive = true;

        edPhone.requestFocus();
        showKeyBoard();


        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edPhone.getText().length() >= 8) {

                    hideSoftKeyboard(getActivity());
                    checkPhone(edPhone.getText().toString());
                }
                else
                {
                    showToast("Số điện thoại nhiều hơn 8 số");
                }

            }
        });

        edPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (edPhone.getText().length() >= 8) {
                    tvNext.setTextColor(getResources().getColor(R.color.xamdam));
                } else {
                    if (tvNext.getCurrentTextColor() == getResources().getColor(R.color.xamdam)) {
                        tvNext.setTextColor(getResources().getColor(R.color.xamnhe));
                    }
                }

            }
        });
    }


    void checkPhone(String phone) {
        ApiHandle.checkPhone(API_CHECKPHONE, phone, this, getActivity());

        mView = new CatLoadingView();
        mView.setCancelable(false);
        // mView.setCanceledOnTouchOutside(false);
        mView.show(getFragmentManager(), "");
    }

    @Override
    public void onProgress(String api) {


    }

    @Override
    public void onComplete(String api, String result, Object extra) {


        if (isAlive) {

            mView.dismiss();

            if (result != null) {

                try {
                    JSONObject jsonObject = new JSONObject(result);


                    if (isDoesString(jsonObject, "status"))
                        status = jsonObject.getString("status");

                    if (status.equals("verified")) {

                        sendBoardCast(BROADLOGIN, INPUTPHONESUCCESS, edPhone.getText().toString());

                    } else if (status.equals("unregistered")) {

                        sendBoardCast(BROADLOGIN, REGISTER, edPhone.getText().toString());

                    } else if (status.equals("unverified")) {

                       sendBoardCast(BROADLOGIN, INPUTPHONESUCCESS, edPhone.getText().toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                showDialogMess(getResources().getString(R.string.coloitrongquatrinh),getActivity());
               // checkPhone(edPhone.getText().toString());
            }
        }
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }

}
