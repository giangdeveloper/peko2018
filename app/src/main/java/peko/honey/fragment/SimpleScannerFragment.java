package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import peko.honey.adapter.FakeorderAdapter;
import peko.honey.adapter.RedeemAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.FakeOrderEntity;
import peko.honey.readToken.WriteReadToken;
import peko.honey.scannner.ZXingScannerView;


public class SimpleScannerFragment extends MyFragment implements ZXingScannerView.ResultHandler, ApiHandle.ApiCallback, ApiConstants {
    private ZXingScannerView mScannerView;
    int service, subtotal, price;
    String name;
    FakeOrderEntity entity;
    FakeorderAdapter adapter;
    ArrayList<FakeOrderEntity> results = new ArrayList<>();
    boolean isAlive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mScannerView = new ZXingScannerView(getActivity());
        return mScannerView;

    }

    @Override
    public void onResume() {
        super.onResume();
        isAlive = true;
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(getActivity(), "Nội dung = " + rawResult.getText() +
//                ", Định dạng = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
//

      //  getOrderfake();


        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                mScannerView.resumeCameraPreview(SimpleScannerFragment.this);
            }
        }, 2000);
    }

    @Override
    public void onPause() {
        super.onPause();
        isAlive = false;
        mScannerView.stopCamera();
    }

    void getOrderfake() {
        ApiHandle.getOrderfake(API_FAKEORDER, this, getActivity());
    }

    void getOrderfake1() {
        ApiHandle.getOrderfake(API_FAKE1, this, getActivity());
    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (api.equals(API_FAKEORDER)) {
                if (result != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        JSONObject jsonObjectData = new JSONObject(jsonObject.getString("data"));
                        JSONArray jsonArrayitems = new JSONArray(jsonObjectData.getString("items"));

                        for (int i = 0; i < jsonArrayitems.length(); i++) {
                            JSONObject jsonObject1 = jsonArrayitems.getJSONObject(i);
                            price = jsonObject1.getInt("price");
                            name = jsonObject1.getString("name");

                            entity = new FakeOrderEntity(price, name);
                            results.add(entity);
                        }

                        service = jsonObjectData.getInt("service");
                        subtotal = jsonObjectData.getInt("subtotal");

                        showDialogRedeem(service, subtotal);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (api.equals(API_FAKE1)) {
                        if (result == null)
                            getOrderfake1();
                    }
                }
            }
            else if(api.equals(API_FAKE1))
            {
                if(result==null)
                    getOrderfake1();
            }
        }

    }


    public void showDialogRedeem(int money, int total) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.fake_dialog);

        RecyclerView rcRedeem = (RecyclerView) dialog.findViewById(R.id.rcRedeem);
        TextView tvMoney = (TextView) dialog.findViewById(R.id.tvMoney);
        TextView tvTotal = (TextView) dialog.findViewById(R.id.tvTotal);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);

        tvMoney.setText(money + " VND");
        tvTotal.setText(total + " VND");

        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcRedeem.setLayoutManager(mLayoutManager);

        adapter = new FakeorderAdapter(getActivity(), results);
        rcRedeem.setAdapter(adapter);

        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                showDialogMess("Paid Successfuly", getActivity());
                getOrderfake1();
            }
        });

        dialog.show();


    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }

}
