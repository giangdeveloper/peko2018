package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import peko.honey.Main2Activity;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.adapter.ExchangeGiftAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.ExChangeGiftEntity;
import peko.honey.onclick.OnclickExchangeGift;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/17/17.
 */

public class ExchangeGiftFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback,OnclickExchangeGift {
    View root;
    public static String TAG = "ExchangeGiftFragment";
    ExchangeGiftAdapter adapter;
    ExChangeGiftEntity entity;
    ArrayList<ExChangeGiftEntity> results = new ArrayList<>();
    ShimmerRecyclerView rcExchangeGift;
    LinearLayoutManager mLayoutManager;
    GridLayoutManager lLayout;
    Main2Activity activity;
    TextView tvExchangeRate, tvDiamond, tvTittle;
    int diamond, exchagerate, id, requirePoints, idShop;
    String nameShop, cover, name;
    boolean isAlive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.exchangegift_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcExchangeGift = (ShimmerRecyclerView) findViewById(R.id.rcExchangeGift);
        tvExchangeRate = (TextView) findViewById(R.id.tvExchangeRate);
        tvDiamond = (TextView) findViewById(R.id.tvDiamond);
        tvTittle = (TextView) findViewById(R.id.tvTittle);
        activity = (Main2Activity) getActivity();
        activity.refreshActionBar(getView());


        diamond = getArguments().getInt("diamond");
        exchagerate = getArguments().getInt("rate");
        nameShop = getArguments().getString("name");
        idShop = getArguments().getInt("id");

        tvDiamond.setText(String.valueOf(diamond));
        tvExchangeRate.setText("(" + IntToStringNoDecimal(exchagerate) + " " + getResources().getString(R.string.sotiendoiqua)+ ")");
        tvTittle.setText(nameShop);

        isAlive = true;

        getExchangeGift();


    }

    void getExchangeGift() {
        ApiHandle.getExchangeGift(API_DETAILSHOP + idShop, this, getActivity());
    }

    @Override
    public void onProgress(String api) {
        rcExchangeGift.showShimmerAdapter();
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (result != null) {

                rcExchangeGift.hideShimmerAdapter();

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray jsonArrayCardRewards = new JSONArray(jsonObject.getString("cardRewards"));
                    for (int i = 0; i < jsonArrayCardRewards.length(); i++) {

                        JSONObject jsonObjectCard = jsonArrayCardRewards.getJSONObject(i);

                        if (isDoesString(jsonObjectCard, "id"))
                            id = jsonObjectCard.getInt("id");

                        if (isDoesString(jsonObjectCard, "name"))
                            name = jsonObjectCard.getString("name");

                        if (isDoesString(jsonObjectCard, "image"))
                            cover = jsonObjectCard.getString("image");

                        if (isDoesString(jsonObjectCard, "requirePoints"))
                            requirePoints = jsonObjectCard.getInt("requirePoints");


                        entity = new ExChangeGiftEntity(id, cover, name, requirePoints);
                        results.add(entity);
                    }

                    lLayout = new GridLayoutManager(getActivity(), 2);
                    rcExchangeGift.setHasFixedSize(true);

                    rcExchangeGift.setLayoutManager(lLayout);

                    adapter = new ExchangeGiftAdapter(getContext(), results,this);
                    rcExchangeGift.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                getExchangeGift();
            }
        }


    }

    @Override
    public void onclickExchangeGift(View v, ExChangeGiftEntity entity) {

    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
