package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import peko.honey.adapter.BuyVoucherAdapter;
import peko.honey.adapter.VoucherHistoryAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.BuyVoucherEntity;
import peko.honey.entity.VoucherHistoryEntity;
import peko.honey.onclick.OnclickVoucherHistory;

/**
 * Created by macshop1 on 12/5/17.
 */

public class VoucherHistoryFragment extends MyFragment implements ApiConstants,ApiHandle.ApiCallback,OnclickVoucherHistory{
    public static String TAG = "VoucherHistoryFragment";
    View root;
    VoucherHistoryEntity entity;
    VoucherHistoryAdapter voucherHistoryAdapter;
    ArrayList<VoucherHistoryEntity> results = new ArrayList<>();
    ShimmerRecyclerView rcVoucherHistory;
    LinearLayoutManager mLayoutManager;
    int idVoucher, requirePoint, totalUsingTime;
    String nameVoucher, desVoucher, coverVoucher, nameShop;
    boolean isAlive;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.voucher_history_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        rcVoucherHistory = (ShimmerRecyclerView)findViewById(R.id.rcVoucherHistory);
        isAlive = true;

        getVoucherHistory();

    }

    void getVoucherHistory() {
        ApiHandle.getMyVoucher(API_VOUCHERHISTORY, this, getActivity());
    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

      if(isAlive)
      {
          if (api.equals(API_VOUCHERHISTORY)) {
              if (result != null) {

                  rcVoucherHistory.hideShimmerAdapter();

//                if (isRefresh == true) {
//                    isRefresh = false;
//                    swipeRefreshLayout.setRefreshing(false);
//                }

                  try {
                      JSONObject jsonObject = new JSONObject(result);

                      JSONArray jsonArrayVoucher = new JSONArray(jsonObject.getString("vouchers"));
                      for (int i = 0; i < jsonArrayVoucher.length(); i++) {
                          JSONObject jsonObjectVoucher = jsonArrayVoucher.getJSONObject(i);


                          JSONObject jsonObjectShop = new JSONObject(jsonObjectVoucher.getString("shop"));

                          if (isDoesString(jsonObjectVoucher, "id"))
                              idVoucher = jsonObjectVoucher.getInt("id");

                          if (isDoesString(jsonObjectVoucher, "requirePoint"))
                              requirePoint = jsonObjectVoucher.getInt("requirePoint");

                          if (isDoesString(jsonObjectVoucher, "totalUsingTime"))
                              totalUsingTime = jsonObjectVoucher.getInt("totalUsingTime");

                          if (isDoesString(jsonObjectShop, "name"))
                              nameShop = jsonObjectShop.getString("name");

                          if (isDoesString(jsonObjectVoucher, "name"))
                              nameVoucher = jsonObjectVoucher.getString("name");

                          if (isDoesString(jsonObjectVoucher, "media"))
                              coverVoucher = jsonObjectVoucher.getString("media");

                          if (isDoesString(jsonObjectVoucher, "description"))
                              desVoucher = jsonObjectVoucher.getString("description");

//                        if (isDoesString(jsonObjectVoucher, "description"))
//
//                            desVoucher += (jsonObjectVoucher.getString("description") + "\n \n");



                          entity = new VoucherHistoryEntity(idVoucher, nameVoucher, nameShop, desVoucher, coverVoucher, requirePoint, totalUsingTime);
                          results.add(entity);

                      }

                      mLayoutManager = new LinearLayoutManager(getActivity());
                      mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                      rcVoucherHistory.setLayoutManager(mLayoutManager);

                      voucherHistoryAdapter = new VoucherHistoryAdapter(getActivity(), results,this);
                      rcVoucherHistory.setAdapter(voucherHistoryAdapter);

                  } catch (JSONException e) {
                      e.printStackTrace();
                  }
              } else {

                  getVoucherHistory();
              }
          }
      }
    }

    @Override
    public void onclickViewLocation(View v, VoucherHistoryEntity entity) {
        String str = "#";
        String str1 = "\n \n";
        showDialogShowLocation(entity.getDes().replaceAll(str, str1));
    }


    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
