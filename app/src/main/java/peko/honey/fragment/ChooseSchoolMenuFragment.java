//package peko.honey.fragment;
//
//import android.app.Dialog;
//import android.content.SharedPreferences;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.AutoCompleteTextView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.peko.neko.pekoallnews.R;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.Normalizer;
//import java.util.ArrayList;
//
//import peko.honey.adapter.SeachSchoolAdapter;
//import peko.honey.api.ApiConstants;
//import peko.honey.api.ApiHandle;
//import peko.honey.common.MyFragment;
//import peko.honey.entity.SeachSchoolEntity;
//import peko.honey.readToken.WriteReadToken;
//
///**
// * Created by macshop1 on 12/6/17.
// */
//
//public class ChooseSchoolMenuFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {
//    public static String TAG = "ChooseSchoolMenuFragment";
//    View root;
//    SeachSchoolAdapter seachSchoolAdapter;
//    SeachSchoolEntity seachSchoolEntity;
//    ArrayList<SeachSchoolEntity> resultsSearch = new ArrayList<>();
//    int currentCode, code;
//    String currentNameSchool, nameSchool;
//    public static final String ACCESS_PREFERENCES = "CheckAccess";
//    AutoCompleteTextView atSearchSchool;
//    LinearLayout linerSchool;
//    TextView tvPoinSchool, tvSchoolName, tvBack, tvAccept;
//    boolean isAlive;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root = inflater.inflate(R.layout.choose_school_dialog, container, false);
//        return root;
//    }
//
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//        atSearchSchool = (AutoCompleteTextView) findViewById(R.id.atSearchSchool);
//        linerSchool = (LinearLayout) findViewById(R.id.linerSchool);
//        tvPoinSchool = (TextView) findViewById(R.id.tvPoinSchool);
//        tvSchoolName = (TextView) findViewById(R.id.tvSchoolName);
//        tvBack = (TextView) findViewById(R.id.tvBack);
//        tvAccept = (TextView) findViewById(R.id.tvAccept);
//
//        isAlive = true;
//
//        atSearchSchool.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    //  sendPoinSchool(WriteReadToken.id, currentCode);
//                    // dialog.dismiss();
//                    return true;
//                }
//                return false;
//            }
//        });
//
//
//    }
//
//    void sendPoinSchool(int id, int code) {
//        ApiHandle.sendPoinSchool(API_SENDSCHOOL_POIN, id, code, this, getContext());
//    }
//
//
//    public void showDialogChooseSchool() {
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
//        if (dialog.getWindow() != null)
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setContentView(R.layout.choose_school_dialog);
//        AutoCompleteTextView atSearchSchool = (AutoCompleteTextView) dialog.findViewById(R.id.atSearchSchool);
//        LinearLayout linerSchool = (LinearLayout) dialog.findViewById(R.id.linerSchool);
//        TextView tvPoinSchool = (TextView) dialog.findViewById(R.id.tvPoinSchool);
//        TextView tvSchoolName = (TextView) dialog.findViewById(R.id.tvSchoolName);
//        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
//        TextView tvAccept = (TextView) dialog.findViewById(R.id.tvAccept);
//        seachSchoolAdapter = new SeachSchoolAdapter(this, R.layout.item_searchschool, resultsSearch);
//        atSearchSchool.setAdapter(seachSchoolAdapter);
//
//        atSearchSchool.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                currentCode = resultsSearch.get(i).getCode();
//                currentNameSchool = resultsSearch.get(i).getName();
//                System.out.println("code : " + currentCode);
//            }
//        });
//
//
//        tvBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//        atSearchSchool.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    //  sendPoinSchool(WriteReadToken.id, currentCode);
//                    // dialog.dismiss();
//                    return true;
//                }
//                return false;
//            }
//        });
//
//        tvAccept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (currentCode != 0) {
//                    sendPoinSchool(WriteReadToken.id, currentCode);
//
//
//                    SharedPreferences settings = getContext().getSharedPreferences(ACCESS_PREFERENCES, getContext().MODE_PRIVATE);
//                    SharedPreferences.Editor prefEditor = settings.edit();
//                    prefEditor.putString("schoolName", currentNameSchool);
//                    prefEditor.commit();
//
//                    dialog.dismiss();
//                } else {
//                    dialog.dismiss();
//                }
//            }
//        });
//
//        dialog.show();
//
//
//    }
//
//    @Override
//    public void onProgress(String api) {
//
//    }
//
//    @Override
//    public void onComplete(String api, String result, Object extra) {
//
//        if (isAlive == true) {
//            if (api.equals(API_GETSCHOOL)) {
//                if (result != null) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//                        JSONArray jsonArraySchools = new JSONArray(jsonObject.getString("schools"));
//                        for (int i = 0; i < jsonArraySchools.length(); i++) {
//                            JSONObject jsonObjectSchool = jsonArraySchools.getJSONObject(i);
//
//                            if (isDoesString(jsonObjectSchool, "code"))
//                                code = jsonObjectSchool.getInt("code");
//
//                            if (isDoesString(jsonObjectSchool, "name"))
//                                nameSchool = jsonObjectSchool.getString("name");
//
//                            seachSchoolEntity = new SeachSchoolEntity(code, nameSchool, stripAccents(nameSchool));
//                            resultsSearch.add(seachSchoolEntity);
//
//                        }
//
//                        seachSchoolAdapter = new SeachSchoolAdapter(getContext(), R.layout.item_searchschool, resultsSearch);
//                        atSearchSchool.setAdapter(seachSchoolAdapter);
//
//                        atSearchSchool.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                currentCode = resultsSearch.get(i).getCode();
//                                currentNameSchool = resultsSearch.get(i).getName();
//                                System.out.println("code : " + currentCode);
//                            }
//                        });
//
//
//                        if (WriteReadToken.success == null) {
//                            SharedPreferences settings = getContext().getSharedPreferences(ACCESS_PREFERENCES, getContext().MODE_PRIVATE);
//                            SharedPreferences.Editor prefEditor = settings.edit();
//                            prefEditor.putString("success", "success");
//                            prefEditor.commit();
//                            WriteReadToken writeReadToken = new WriteReadToken();
//                            writeReadToken.getPref("CheckAccess", getContext());
//                        }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else if (api.equals(API_SENDSCHOOL_POIN)) {
//                if (result != null) {
//
//
//                    SharedPreferences settings = getContext().getSharedPreferences(ACCESS_PREFERENCES, getContext().MODE_PRIVATE);
//                    SharedPreferences.Editor prefEditor = settings.edit();
//                    prefEditor.putString("success", "success");
//                    prefEditor.commit();
//                    WriteReadToken writeReadToken = new WriteReadToken();
//                    writeReadToken.getPref("CheckAccess", getContext());
//                } else {
//
//                }
//
//            } else {
//
//                sendPoinSchool(WriteReadToken.id, code);
//            }
//        }
//    }
//
//
//    public static String stripAccents(String s) {
//        s = Normalizer.normalize(s, Normalizer.Form.NFD);
//        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
//        s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
//        //System.out.println(s);
//        return s;
//    }
//}
