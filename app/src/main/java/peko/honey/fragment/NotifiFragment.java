package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import peko.honey.Main2Activity;
import peko.honey.adapter.NotifiAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.NotifiEntity;

/**
 * Created by macshop1 on 11/27/17.
 */

public class NotifiFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {

    public static String TAG = "NotifiFragment";
    View root;
    ShimmerRecyclerView rcNotifi;
    int type, poin;
    String name, nameType2, nameVoucher, avatar, time;
    String heading, content;
    NotifiEntity notifiEntity;
    NotifiAdapter adapter;
    LinearLayoutManager mLayoutManager;
    ArrayList<NotifiEntity> results = new ArrayList<>();
    Main2Activity activity;
    TextView tvTittle;
    boolean isAlive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.notifi_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcNotifi = (ShimmerRecyclerView) findViewById(R.id.rcNotifi);
        tvTittle = (TextView)findViewById(R.id.tvTittle);
        activity = (Main2Activity) getActivity();

        tvTittle.setText(getResources().getString(R.string.thongbao));
        activity.refreshActionBar(getView());

        isAlive = true;

        getNotifi();

    }

    void getNotifi() {
        ApiHandle.getNotifi(API_NOTIFI, this, getActivity());
    }

    @Override
    public void onProgress(String api) {
        rcNotifi.showShimmerAdapter();
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (api.equals(API_NOTIFI)) {
                if (result != null) {

                    rcNotifi.hideShimmerAdapter();
                    try {

                        JSONObject jsonObject = new JSONObject(result);


                        JSONArray jsonArrayNotifications = new JSONArray(jsonObject.getString("notifications"));

                        for (int i = 0; i < jsonArrayNotifications.length(); i++) {

                            JSONObject jsonObjectNotifi = jsonArrayNotifications.getJSONObject(i);

                            type = jsonObjectNotifi.getInt("type");


                            if (type == 1) {

                                JSONObject jsonObjectData = new JSONObject(jsonObjectNotifi.getString("data"));
                                JSONObject jsonObjectShop = new JSONObject(jsonObjectData.getString("shop"));

                                if (isDoesString(jsonObjectShop, "name"))
                                    name = jsonObjectShop.getString("name");

                                if (isDoesString(jsonObjectShop, "avatar"))
                                    avatar = jsonObjectShop.getString("avatar");

                                if (isDoesString(jsonObjectData, "collectedAt"))
                                    time = parseDateToddMMyyyyy(jsonObjectData.getString("collectedAt"));

                                if (isDoesString(jsonObjectShop, "receivedPoints"))
                                    poin = jsonObjectShop.getInt("receivedPoints");

                            } else if (type == 2) {

                                JSONObject jsonObjectData = new JSONObject(jsonObjectNotifi.getString("data"));
                                JSONObject jsonObjectShop = new JSONObject(jsonObjectData.getString("shop"));
                                JSONObject jsonObjectReward = new JSONObject(jsonObjectData.getString("reward"));

                                if (isDoesString(jsonObjectShop, "name"))
                                    name = jsonObjectShop.getString("name");

                                if (isDoesString(jsonObjectShop, "avatar"))
                                    avatar = jsonObjectShop.getString("avatar");

                                if (isDoesString(jsonObjectData, "collectedAt"))
                                    time = parseDateToddMMyyyyy(jsonObjectData.getString("collectedAt"));

                                if (isDoesString(jsonObjectReward, "name"))
                                    nameType2 = jsonObjectReward.getString("name");


                            } else if (type == 3) {

                                JSONObject jsonObjectData = new JSONObject(jsonObjectNotifi.getString("data"));

                                if (isDoesString(jsonObjectData, "heading"))
                                    heading = jsonObjectData.getString("heading");

                                if (isDoesString(jsonObjectData, "content"))
                                    content = jsonObjectData.getString("content");

                            } else if (type == 6) {

                                JSONObject jsonObjectData = new JSONObject(jsonObjectNotifi.getString("data"));
                                JSONObject jsonObjectVoucher = new JSONObject(jsonObjectData.getString("voucher"));
                                JSONObject jsonObjectShop = new JSONObject(jsonObjectVoucher.getString("shop"));


                                if (isDoesString(jsonObjectVoucher, "name"))
                                    nameVoucher = jsonObjectVoucher.getString("name");

                                if (isDoesString(jsonObjectData, "createdAt"))
                                    time = parseDateToddMMyyyyy(jsonObjectData.getString("createdAt"));

                                if (isDoesString(jsonObjectShop, "name"))
                                    name = jsonObjectShop.getString("name");


                                if (isDoesString(jsonObjectShop, "avatar"))
                                    avatar = jsonObjectShop.getString("avatar");

                            }

                            notifiEntity = new NotifiEntity(0, type, poin, name, nameType2, nameVoucher, avatar, time, content, heading);
                            results.add(notifiEntity);
                        }

                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rcNotifi.setLayoutManager(new LinearLayoutManager(getActivity()));
                        adapter = new NotifiAdapter(getActivity(), results);
                        rcNotifi.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                    getNotifi();
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
