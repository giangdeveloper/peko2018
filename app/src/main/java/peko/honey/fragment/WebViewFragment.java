package peko.honey.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;


import com.peko.neko.pekoallnews.R;

import peko.honey.Main2Activity;
import peko.honey.MainActivity;
import peko.honey.common.MyFragment;

/**
 * Created by Giang on 3/13/2017.
 */

public class WebViewFragment extends MyFragment {
    View root;
    public static String TAG = "WebViewFragment";
    WebView webView;
    Bundle b;
    TextView tvWebView;
    Main2Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub


        root = inflater.inflate(R.layout.webview_fragment, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

        b = getArguments();
        webView = (WebView) findViewById(R.id.webView);
        tvWebView = (TextView) findViewById(R.id.tvTittle);
        activity = (Main2Activity) getActivity();

        activity.refreshActionBar(getView());
        tvWebView.setText("Chiến dịch tháng 12");

        WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);

        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
               dismissLoadingDialog();

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }
        });
        webView.loadUrl("http://pekocampaign.com/");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

}
