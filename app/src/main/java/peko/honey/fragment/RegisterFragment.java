package peko.honey.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.LoginActivity;
import peko.honey.Main2Activity;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;

/**
 * Created by macshop1 on 11/29/17.
 */

public class RegisterFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {
    public static String TAG = "RegisterFragment";
    View root;
    LoginActivity activity;
    EditText edUserName, edPhone, edPass;
    TextView tvRegister;
    boolean isAlive;
    String phone, token, username, email, avatar, fullName, referralCode;
    int id, totalPoints;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    CatLoadingView mView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.register_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activity = (LoginActivity) getActivity();
        edUserName = (EditText) findViewById(R.id.edUserName);
        edPhone = (EditText) findViewById(R.id.edPhone);
        edPass = (EditText) findViewById(R.id.edPass);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        activity.refreshActionBar(getView());

        isAlive = true;

        edUserName.requestFocus();
        showKeyBoard();

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!hasPermissions(getActivity(), PERMISSIONS)) {
                    ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
                }
                else
                {
                    registerAccount();
                }

            }
        });
    }


    void registerAccount() {

        String name = edUserName.getText().toString();
        String phone = edPhone.getText().toString();
        String pass = edPass.getText().toString();


        if (name.matches("") | name.trim().length() <= 0) {
            showToast("Mời bạn nhập email");
            return;
        }

        if (phone.matches("") | phone.trim().length() <= 0) {
            showToast("Mời bạn nhập số điện thoại");
            return;
        }

        if (phone.matches("") | phone.trim().length() <= 9) {
            showToast("Số điện thoại phải nhiều hơn 9 ký tự");
            return;
        }

        if (pass.matches("") | pass.trim().length() <= 0) {
            showToast("Mời bạn nhập mật khẩu");
            return;
        }

        if ( pass.trim().length() <= 7) {
            showToast("Mật khẩu phải lớn hơn 7 ký tự");
            return;
        }

        signUp(name, phone, pass);

    }

    void signUp(String name, String phone, String pass) {
        ApiHandle.signUp(API_SIGNUP, this, getActivity(), name, phone, pass);

        mView = new CatLoadingView();
        mView.setCancelable(false);
        // mView.setCanceledOnTouchOutside(false);
        mView.show(getFragmentManager(), "");
    }

    @Override
    public void onProgress(String api) {


    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if(isAlive)
        {
            mView.dismiss();

            if(result!=null)
            {


                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (isDoesString(jsonObject, "status")) {
                        showToast(jsonObject.getString("message"));
                    } else {

                        JSONObject jsonObjectUser = new JSONObject(jsonObject.getString("user"));

                        if (isDoesString(jsonObject, "token"))
                            token = jsonObject.getString("token");

                        if (isDoesString(jsonObjectUser, "id"))
                            id = jsonObjectUser.getInt("id");

                        if (isDoesString(jsonObjectUser, "username"))
                            username = jsonObjectUser.getString("username");

                        if (isDoesString(jsonObjectUser, "email"))
                            email = jsonObjectUser.getString("email");

                        if (isDoesString(jsonObjectUser, "phone"))
                            phone = jsonObjectUser.getString("phone");

                        if (isDoesString(jsonObjectUser, "avatar"))
                            avatar = jsonObjectUser.getString("avatar");

                        if (isDoesString(jsonObjectUser, "fullName"))
                            fullName = jsonObjectUser.getString("fullName");


                        if (isDoesString(jsonObjectUser, "referralCode"))
                            referralCode = jsonObjectUser.getString("referralCode");

                        if (isDoesString(jsonObjectUser, "totalPoints"))
                            totalPoints = jsonObjectUser.getInt("totalPoints");

                        pref = getActivity().getSharedPreferences("CheckAccess", 0);// 0 - là chế độ private
                        editor = pref.edit();
                        editor.putString("Token", token);
                        editor.putInt("id", id);
                        editor.putString("username", username);
                        editor.putString("email", email);
                        editor.putString("phone", edPhone.getText().toString());
                        editor.putString("avatar", avatar);
                        editor.putString("fullName", fullName);
                        editor.putString("referralCode", referralCode);
                        editor.putInt("totalPoints", totalPoints);
                        editor.commit();

                      //  sendBoardCast(BROADLOGIN,CHOOSESCHOOL);

                        Intent intent = new Intent(getActivity(), Main2Activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                showDialogMess(getResources().getString(R.string.coloitrongquatrinh),getActivity());
            }
        }


    }

    //check android 6.0
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
