package peko.honey.fragment;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.Main2Activity;
import peko.honey.adapter.BuyVoucherAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.BuyVoucherEntity;
import peko.honey.onclick.OnclickBuyvoucher;
import peko.honey.readToken.WriteReadToken;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/18/17.
 */

public class BuyVoucherFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback, OnclickBuyvoucher, SwipeRefreshLayout.OnRefreshListener {

    public static String TAG = "BuyVoucherFragment";
    View root;
    BuyVoucherAdapter adapter;
    BuyVoucherEntity entity;
    ArrayList<BuyVoucherEntity> results = new ArrayList<>();
    ShimmerRecyclerView rcBuyVoucher;
    LinearLayoutManager mLayoutManager;
    int isBuy, idVoucher, requirePoint, totalUsingTime, currentPoin, idCurrentVoucher, status;
    String nameVoucher, desVoucher, coverVoucher, currentNameVoucher, nameShop;
    boolean isAlive, isRefresh;
    Main2Activity activity;
    TextView tvTittle, tvPoin;
    public static final String ACCESS_PREFERENCES = "CheckAccess";
    CatLoadingView mView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.buyvoucher_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        rcBuyVoucher = (ShimmerRecyclerView) findViewById(R.id.rcBuyVoucher);
        tvPoin = (TextView) findViewById(R.id.tvPoin);
        activity = (Main2Activity) getActivity();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        isAlive = true;

        isBuy = getArguments().getInt("check", 0);
        tvPoin.setText(String.valueOf(WriteReadToken.totalPoints));

        if (isBuy == 1) {
            findViewById(R.id.headerVoucher).setVisibility(View.VISIBLE);
            tvTittle = (TextView) findViewById(R.id.tvTittle);
            activity.refreshActionBar(getView());

            tvTittle.setText(getResources().getString(R.string.voucher));
        }

        swipeRefreshLayout.setOnRefreshListener(this);


        getBuyVoucher();
    }

    void getBuyVoucher() {
        ApiHandle.getBuyVoucher(API_BUYVOUCHER, this, getActivity());
    }

    void buyVoucher() {
        ApiHandle.buyVoucher(API_GETBUYVOUCHER + idCurrentVoucher + BUY, this, getActivity());
    }

    void reLoadVoucher() {
        results = new ArrayList<>();
        //  adapter = new MyVoucherAdapter(getActivity(), results, this);
        // rcMyVoucher.setAdapter(adapter);

        getBuyVoucher();
    }

    @Override
    public void onProgress(String api) {
        if (api.equals(API_BUYVOUCHER))
            rcBuyVoucher.showShimmerAdapter();
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (api.equals(API_BUYVOUCHER)) {
                if (result != null) {

                    rcBuyVoucher.hideShimmerAdapter();

                    if (isRefresh == true) {
                        isRefresh = false;
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        JSONArray jsonArrayVoucher = new JSONArray(jsonObject.getString("vouchers"));
                        for (int i = 0; i < jsonArrayVoucher.length(); i++) {
                            JSONObject jsonObjectVoucher = jsonArrayVoucher.getJSONObject(i);


                            JSONObject jsonObjectShop = new JSONObject(jsonObjectVoucher.getString("shop"));

                            if (isDoesString(jsonObjectVoucher, "id"))
                                idVoucher = jsonObjectVoucher.getInt("id");

                            if (isDoesString(jsonObjectVoucher, "requirePoint"))
                                requirePoint = jsonObjectVoucher.getInt("requirePoint");

                            if (isDoesString(jsonObjectVoucher, "totalUsingTime"))
                                totalUsingTime = jsonObjectVoucher.getInt("totalUsingTime");

                            if (isDoesString(jsonObjectShop, "name"))
                                nameShop = jsonObjectShop.getString("name");

                            if (isDoesString(jsonObjectVoucher, "name"))
                                nameVoucher = jsonObjectVoucher.getString("name");

                            if (isDoesString(jsonObjectVoucher, "media"))
                                coverVoucher = jsonObjectVoucher.getString("media");

                            if (isDoesString(jsonObjectVoucher, "description"))
                                desVoucher = jsonObjectVoucher.getString("description");


                            entity = new BuyVoucherEntity(idVoucher, nameVoucher, nameShop, desVoucher, coverVoucher, requirePoint, totalUsingTime);
                            results.add(entity);
                        }

                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        rcBuyVoucher.setLayoutManager(mLayoutManager);

                        adapter = new BuyVoucherAdapter(getActivity(), results, this);
                        rcBuyVoucher.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    getBuyVoucher();
                }
            } else if (api.equals(API_GETBUYVOUCHER + idCurrentVoucher + BUY)) {


                mView.dismiss();

                if (result != null) {


                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "status"))
                            status = jsonObject.getInt("status");

                        if (status == 200) {

                            // showDialogMess("Bạn đã mua voucher " + currentNameVoucher + " thành công", getActivity());
                            SharedPreferences settings = getActivity().getSharedPreferences(ACCESS_PREFERENCES, getActivity().MODE_PRIVATE);
                            SharedPreferences.Editor prefEditor = settings.edit();
                            prefEditor.putInt("totalPoints", WriteReadToken.totalPoints - currentPoin);
                            prefEditor.commit();

                            WriteReadToken writeReadToken = new WriteReadToken();
                            writeReadToken.getPref("CheckAccess", getActivity());

                            activity.reloadPoin();

                            sendBoardCast(BROADVOUCHER, RELOADPOIN);

                        } else {
                            showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                }
            }
        }
    }

    @Override
    public void onclickBuyVoucher(View v, BuyVoucherEntity entity) {

        if (WriteReadToken.totalPoints >= entity.getRequirePointl()) {
            currentPoin = entity.getRequirePointl();
            currentNameVoucher = entity.getName();
            idCurrentVoucher = entity.getId();

            showDialogBuyVoucher(getResources().getString(R.string.vouchertext) + " " + entity.getName(), getResources().getString(R.string.voigia) + entity.getRequirePointl() + " " + getResources().getString(R.string.pekocoin));

        } else {
            showToast("Số điểm của bạn không đủ để mua Voucher");
        }

    }

    @Override
    public void onclickViewLocation(View v, BuyVoucherEntity entity) {
        String str = "#";
        String str1 = "\n \n";
        showDialogShowLocation(entity.getDes().replaceAll(str, str1));

    }


    public void showDialogBuyVoucher(String text, String Cost) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.buyvoucher_confirm_dialog);


        TextView tvTextVoucher = (TextView) dialog.findViewById(R.id.tvTextVoucher);
        TextView tvCost = (TextView) dialog.findViewById(R.id.tvCost);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        RelativeLayout relayX = (RelativeLayout) dialog.findViewById(R.id.relayX);

        tvTextVoucher.setText(Html.fromHtml(getColoredSpanned(text)));
        tvCost.setText(Html.fromHtml(getColoredSpanned(Cost)));

        relayX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                mView = new CatLoadingView();
                mView.setCancelable(false);
                // mView.setCanceledOnTouchOutside(false);
                mView.show(getFragmentManager(), "");

                buyVoucher();
            }
        });

        dialog.show();


    }

    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        isRefresh = true;
        reLoadVoucher();
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}