package peko.honey.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.adapter.MyVoucherAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.MyVoucherEntity;
import peko.honey.onclick.OnclickUsevoucher;
import peko.honey.readToken.WriteReadToken;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/18/17.
 */

public class MyVoucherFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback, OnclickUsevoucher, SwipeRefreshLayout.OnRefreshListener {

    public static String TAG = "MyVoucherFragment";
    View root;
    MyVoucherAdapter adapter;
    MyVoucherEntity entity;
    ShimmerRecyclerView rcMyVoucher;
    ArrayList<MyVoucherEntity> results = new ArrayList<>();

    LinearLayoutManager mLayoutManager;
    int idVoucher, discountValue, currentIdVoucher, currentPostVoucher;
    String nameVoucher, nameShop, desVoucher, coverVoucher, endUsingTime, code, currentCode, currentNameVoucher;
    boolean isAlive, isRefresh, isReload;
    CatLoadingView mView;
    private SwipeRefreshLayout swipeRefreshLayout;
    int page = 1, voucherID;
    boolean firsttime;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.myvoucher_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rcMyVoucher = (ShimmerRecyclerView) findViewById(R.id.rcMyVoucher);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        isAlive = true;

        swipeRefreshLayout.setOnRefreshListener(this);

        getMyVoucher1(page);


    }

    void getMyVoucher() {
        ApiHandle.getMyVoucher(API_MYVOUCHER, this, getActivity());
    }

    void getMyVoucher1(int page) {
        ApiHandle.getMyVoucher1("http://k8s.pekopeko.vn/api/v1/get-vouchers?token=72927d8b-26cc-473b-9c5b-dfb602bf557c&pageidx=", page, this, getActivity());
    }

    void getMyVoucher2(int id) {
        ApiHandle.getMyVoucher1("http://k8s.pekopeko.vn/api/v1/get-code?token=72927d8b-26cc-473b-9c5b-dfb602bf557c&id=", id, this, getActivity());
    }

    void userVoucher(String inputCode) {
        ApiHandle.useVoucher(API_USEVOUCHER + currentIdVoucher + REDEEM, this, getActivity(), currentCode, inputCode);
    }

    void sendSchoolPoin(String id) {
        ApiHandle.sendChoolPoin(API_SETPOIN_SCHOOL, id, this);
    }

    public void reLoadVoucher() {
        results = new ArrayList<>();
        //  adapter = new MyVoucherAdapter(getActivity(), results, this);
        // rcMyVoucher.setAdapter(adapter);

        getMyVoucher1(page);
    }

    public void isReload(boolean reload) {
        isReload = reload;
    }


    @Override
    public void onProgress(String api) {
        if (api.equals(API_MYVOUCHER)) {
            if (isReload == true) {
                isReload = false;

            } else {
                rcMyVoucher.showShimmerAdapter();
            }
        }

        if (api.equals(API_USEVOUCHER + currentIdVoucher + REDEEM)) {
            mView = new CatLoadingView();
            mView.setCancelable(false);
            // mView.setCanceledOnTouchOutside(false);
            mView.show(getFragmentManager(), "");
        }
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {

            if (api.equals("http://k8s.pekopeko.vn/api/v1/get-vouchers?token=72927d8b-26cc-473b-9c5b-dfb602bf557c&pageidx=" + page)) {
                if (result != null) {
                    rcMyVoucher.hideShimmerAdapter();

                    if (isRefresh == true) {
                        isRefresh = false;
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    if (!firsttime) {
                        firsttime = true;

                        try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        JSONArray jsonArrayUserVouchers = new JSONArray(jsonObject.getString("userVouchers"));
//
//                        for (int i = 0; i < jsonArrayUserVouchers.length(); i++) {
//                            JSONObject jsonObjectVoucher = jsonArrayUserVouchers.getJSONObject(i);
//
//
//                            JSONObject jsonObjectMyVoucher = new JSONObject(jsonObjectVoucher.getString("voucher"));
//
//                            JSONObject jsonObjectShop = new JSONObject(jsonObjectMyVoucher.getString("shop"));
//
//                            if (isDoesString(jsonObjectMyVoucher, "id"))
//                                idVoucher = jsonObjectMyVoucher.getInt("id");
//
//                            if (isDoesString(jsonObjectVoucher, "code"))
//                                code = jsonObjectVoucher.getString("code");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "name"))
//                                nameVoucher = jsonObjectMyVoucher.getString("name");
//
//                            if (isDoesString(jsonObjectShop, "name"))
//                                nameShop = jsonObjectShop.getString("name");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "description"))
//                                desVoucher = jsonObjectMyVoucher.getString("description");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "media"))
//                                coverVoucher = jsonObjectMyVoucher.getString("media");
//
//                            if (isDoesString(jsonObjectMyVoucher, "endUsableTime"))
//                                endUsingTime = parseDateToddMMyyyy(jsonObjectMyVoucher.getString("endUsableTime"));
//
//                            if (isDoesString(jsonObjectMyVoucher, "discountValue"))
//                                discountValue = jsonObjectMyVoucher.getInt("discountValue");

                            page += 1;
                            JSONArray jsonArrayVoucher = new JSONArray(result);
                            for (int i = 0; i < jsonArrayVoucher.length(); i++) {

                                JSONObject jsonObject = jsonArrayVoucher.getJSONObject(i);

                                if (isDoesString(jsonObject, "partnerVoucherId"))
                                    idVoucher = Integer.parseInt(jsonObject.getString("partnerVoucherId"));
                                if (isDoesString(jsonObject, "name"))
                                    nameVoucher = jsonObject.getString("name");
                                if (isDoesString(jsonObject, "partnerCode"))
                                    code = jsonObject.getString("partnerCode");
                                if (isDoesString(jsonObject, "shortDescription"))
                                    desVoucher = jsonObject.getString("shortDescription");
                                if (isDoesString(jsonObject, "images"))
                                    coverVoucher = jsonObject.getString("images");
                                if (isDoesString(jsonObject, "endDate"))
                                    endUsingTime = parseDateToddMMyyyy(jsonObject.getString("endDate"));

                                if (isDoesString(jsonObject, "shopInfo"))
                                {
                                    if (jsonObject.getString("shopInfo") != null & !jsonObject.getString("shopInfo").equals("null")) {
                                        JSONObject jsonObjectShopInfo = new JSONObject(jsonObject.getString("shopInfo"));
                                        if (isDoesString(jsonObjectShopInfo, "name"))
                                            nameShop = jsonObjectShopInfo.getString("name");
                                    } else {
                                        nameShop = "Voucher";
                                    }
                                }
                                else
                                {
                                    nameShop = "Voucher";
                                }



                                entity = new MyVoucherEntity(idVoucher, code, discountValue, nameVoucher, nameShop, desVoucher, coverVoucher, endUsingTime, false);
                                results.add(entity);

                            }

                            mLayoutManager = new LinearLayoutManager(getActivity());
                            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rcMyVoucher.setLayoutManager(mLayoutManager);

                            adapter = new MyVoucherAdapter(getActivity(), results, this);
                            rcMyVoucher.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else {

                        try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        JSONArray jsonArrayUserVouchers = new JSONArray(jsonObject.getString("userVouchers"));
//
//                        for (int i = 0; i < jsonArrayUserVouchers.length(); i++) {
//                            JSONObject jsonObjectVoucher = jsonArrayUserVouchers.getJSONObject(i);
//
//
//                            JSONObject jsonObjectMyVoucher = new JSONObject(jsonObjectVoucher.getString("voucher"));
//
//                            JSONObject jsonObjectShop = new JSONObject(jsonObjectMyVoucher.getString("shop"));
//
//                            if (isDoesString(jsonObjectMyVoucher, "id"))
//                                idVoucher = jsonObjectMyVoucher.getInt("id");
//
//                            if (isDoesString(jsonObjectVoucher, "code"))
//                                code = jsonObjectVoucher.getString("code");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "name"))
//                                nameVoucher = jsonObjectMyVoucher.getString("name");
//
//                            if (isDoesString(jsonObjectShop, "name"))
//                                nameShop = jsonObjectShop.getString("name");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "description"))
//                                desVoucher = jsonObjectMyVoucher.getString("description");
//
//
//                            if (isDoesString(jsonObjectMyVoucher, "media"))
//                                coverVoucher = jsonObjectMyVoucher.getString("media");
//
//                            if (isDoesString(jsonObjectMyVoucher, "endUsableTime"))
//                                endUsingTime = parseDateToddMMyyyy(jsonObjectMyVoucher.getString("endUsableTime"));
//
//                            if (isDoesString(jsonObjectMyVoucher, "discountValue"))
//                                discountValue = jsonObjectMyVoucher.getInt("discountValue");
                            page += 1;
                            JSONArray jsonArrayVoucher = new JSONArray(result);
                            for (int i = 0; i < jsonArrayVoucher.length(); i++) {

                                JSONObject jsonObject = jsonArrayVoucher.getJSONObject(i);

                                if (isDoesString(jsonObject, "partnerVoucherId"))
                                    idVoucher = Integer.parseInt(jsonObject.getString("partnerVoucherId"));
                                if (isDoesString(jsonObject, "name"))
                                    nameVoucher = jsonObject.getString("name");
                                if (isDoesString(jsonObject, "partnerCode"))
                                    code = jsonObject.getString("partnerCode");
                                if (isDoesString(jsonObject, "shortDescription"))
                                    desVoucher = jsonObject.getString("shortDescription");
                                if (isDoesString(jsonObject, "images"))
                                    coverVoucher = jsonObject.getString("images");
                                if (isDoesString(jsonObject, "endDate"))
                                    endUsingTime = parseDateToddMMyyyy(jsonObject.getString("endDate"));

                                if (jsonObject.getString("shopInfo") != null & !jsonObject.getString("shopInfo").equals("null")) {
                                    JSONObject jsonObjectShopInfo = new JSONObject(jsonObject.getString("shopInfo"));
                                    if (isDoesString(jsonObjectShopInfo, "name"))
                                        nameShop = jsonObjectShopInfo.getString("name");
                                } else {
                                    nameShop = "Voucher";
                                }


                                entity = new MyVoucherEntity();
                                entity.setIdVoucher(idVoucher);
                                entity.setNameVoucher(nameVoucher);
                                entity.setCode(code);
                                entity.setDesVoucher(desVoucher);
                                entity.setCoverVoucher(coverVoucher);
                                entity.setEndUsingTime(endUsingTime);
                                entity.setNameShop(nameShop);

                                adapter.notifyDataSetChanged();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }


                    rcMyVoucher.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);
                        }

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            int totalItemCount = mLayoutManager.getItemCount();
                            int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                            if (totalItemCount > 1) {
                                if (lastVisibleItem >= totalItemCount - 3) {
//                                        Toast.makeText(getActivity(), "Đang lấy thêm dữ liệu ...", Toast.LENGTH_SHORT).show();
                                    getMyVoucher1(page);

                                }
                            }
                        }
                    });

                } else {
                    getMyVoucher1(page);
                }
            } else if (api.equals(API_USEVOUCHER + currentIdVoucher + REDEEM)) {
                mView.dismiss();
                if (result != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "status")) {
                            if (jsonObject.getInt("status") == 200) {
                                showDialogVoucher("Bạn đã sử dụng voucher " + currentNameVoucher + " thành công", "Mã code : " + currentCode, getActivity());
                                //  reLoadVoucher();

                                results.get(currentPostVoucher).setIsused(true);
                                adapter.notifyDataSetChanged();

                                sendSchoolPoin(String.valueOf(WriteReadToken.id));
                            } else {
                                showDialogMess(jsonObject.getString("message"), getActivity());

                                // results.get(currentPostVoucher).setIsused(true);
                                //  adapter.notifyDataSetChanged();

                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getActivity());
                }
            } else if (api.equals(API_SETPOIN_SCHOOL)) {
                if (result != null) {

                } else {
                    sendSchoolPoin(String.valueOf(WriteReadToken.id));
                }
            } else if (api.equals("http://k8s.pekopeko.vn/api/v1/get-code?token=72927d8b-26cc-473b-9c5b-dfb602bf557c&id=" + voucherID)) {
                if (result != null) {
                    showDialogBuyVoucher(result);

                    mView.dismiss();

                } else {
                    mView.dismiss();
                    getMyVoucher2(voucherID);
                }

        }

    }

}

    @Override
    public void onclickUseVoucher(View v, MyVoucherEntity entity, int pos) {
        currentPostVoucher = pos;
        currentNameVoucher = entity.getNameVoucher();
        currentIdVoucher = entity.getIdVoucher();
        currentCode = entity.getCode();
        voucherID = entity.getIdVoucher();
        getMyVoucher2(entity.getIdVoucher());

        mView = new CatLoadingView();
        mView.setCancelable(false);
        // mView.setCanceledOnTouchOutside(false);
        mView.show(getFragmentManager(), "");

      //  showDialogBuyVoucher(entity.getCode());

    }

    @Override
    public void onclickViewLocation(View v, MyVoucherEntity entity) {

        String str = "#";
        String str1 = "\n \n";
        showDialogShowLocation(entity.getDesVoucher().replaceAll(str, str1));

    }


    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }

    public void showDialogBuyVoucher(String pin) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.buyvoucher_dialog);

        final EditText edEnterPin = (EditText) dialog.findViewById(R.id.edEnterPin);
        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        TextView tvPinCode = (TextView) dialog.findViewById(R.id.tvPinCode);
        ImageView imgProfileBuyVoucher = (ImageView) dialog.findViewById(R.id.imgProfileBuyVoucher);
        TextView tvPhone = (TextView) dialog.findViewById(R.id.tvPhone);
        TextView tvNameBuyVoucher = (TextView) dialog.findViewById(R.id.tvNameBuyVoucher);
        final EditText edInputPin = (EditText) dialog.findViewById(R.id.edInputPin);

        tvNameBuyVoucher.setText(WriteReadToken.fullName);
        tvPinCode.setText("Mã code : " + pin);
        if (WriteReadToken.phone != null & !WriteReadToken.phone.equals("") & !WriteReadToken.phone.isEmpty() & !WriteReadToken.phone.equals("null") & WriteReadToken.verified == true)
            tvPhone.setText("+" + WriteReadToken.phone.substring(0, 2) + " " + WriteReadToken.phone.substring(2, WriteReadToken.phone.length()));

        if (WriteReadToken.avatar != null & !WriteReadToken.avatar.equals("") & !WriteReadToken.avatar.isEmpty()) {
            Picasso.with(getActivity())
                    .load(WriteReadToken.avatar)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(imgProfileBuyVoucher);
        }

//        edEnterPin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//              //  edInputPin.setVisibility(View.VISIBLE);
//                edInputPin.requestFocus();
//                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//
//            }
//        });

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        edEnterPin.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if ((actionId == EditorInfo.IME_ACTION_DONE)) {

                    userVoucher(edEnterPin.getText().toString());

                    dialog.dismiss();
                }
                return false;
            }
        });

        dialog.show();

    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        isRefresh = true;
        reLoadVoucher();
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}