package peko.honey.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.ArrayList;

import peko.honey.Main2Activity;
import peko.honey.adapter.SeachSchoolAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyFragment;
import peko.honey.entity.SeachSchoolEntity;

/**
 * Created by macshop1 on 12/3/17.
 */

public class ChooseSchoolFragment extends MyFragment implements ApiConstants, ApiHandle.ApiCallback {
    public static String TAG = "ChooseSchoolFragment";
    View root;
    AutoCompleteTextView atSearchSchool;
    SeachSchoolAdapter adapter;
    SeachSchoolEntity entity;
    ArrayList<SeachSchoolEntity> results = new ArrayList<>();
    TextView tvLogIn;
    boolean isAlive;
    int code, currentCode;
    String name, currentNameSchool;
    public static final String ACCESS_PREFERENCES = "CheckAccess";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.choose_school_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tvLogIn = (TextView) findViewById(R.id.tvLogIn);
        atSearchSchool = (AutoCompleteTextView) findViewById(R.id.atSearchSchool);
        isAlive = true;
        getSchool();

        tvLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SharedPreferences settings = getActivity().getSharedPreferences(ACCESS_PREFERENCES, getActivity().MODE_PRIVATE);
                SharedPreferences.Editor prefEditor = settings.edit();
                prefEditor.putString("schoolName", currentNameSchool);
                prefEditor.commit();

                Intent intent = new Intent(getActivity(), Main2Activity.class);
                intent.putExtra("code", currentCode);
                intent.putExtra("isnews", 1);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }

    void getSchool() {
        ApiHandle.getSchool(API_GETSCHOOL, this);
    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {
            if (result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArraySchools = new JSONArray(jsonObject.getString("schools"));
                    for (int i = 0; i < jsonArraySchools.length(); i++) {
                        JSONObject jsonObjectSchool = jsonArraySchools.getJSONObject(i);

                        if (isDoesString(jsonObjectSchool, "code"))
                            code = jsonObjectSchool.getInt("code");

                        if (isDoesString(jsonObjectSchool, "name"))
                            name = jsonObjectSchool.getString("name");

                        entity = new SeachSchoolEntity(code, name, stripAccents(name));
                        results.add(entity);
                    }

                    if (isAlive) {
                        adapter = new SeachSchoolAdapter(getContext(), R.layout.item_searchschool, results);
                        atSearchSchool.setAdapter(adapter);

                        atSearchSchool.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                currentCode = results.get(i).getCode();
                                currentNameSchool = results.get(i).getName();
                            }
                        });
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                getSchool();
            }
        }

    }


    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        //System.out.println(s);
        return s;
    }

    @Override
    public void onDestroy() {
        isAlive = false;
        super.onDestroy();
    }
}
