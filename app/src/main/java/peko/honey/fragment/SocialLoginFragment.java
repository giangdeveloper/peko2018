package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peko.neko.pekoallnews.R;

import peko.honey.LoginActivity;
import peko.honey.common.MyFragment;

/**
 * Created by macshop1 on 11/27/17.
 */

public class SocialLoginFragment extends MyFragment {
    public static String TAG = "SocialLoginFragment";
    View root;
    LoginActivity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.social_loginfragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        activity = (LoginActivity)getActivity();
        activity.refreshActionBar(getView());
    }
}
