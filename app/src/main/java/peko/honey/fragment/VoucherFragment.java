package peko.honey.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import peko.honey.Main2Activity;
import com.peko.neko.pekoallnews.R;
import peko.honey.adapter.SampleFragmentPagerAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.common.MyFragment;
import peko.honey.navigator.PagerSlidingTabStrip;
import peko.honey.readToken.WriteReadToken;

import java.util.ArrayList;

/**
 * Created by macshop1 on 11/18/17.
 */

public class VoucherFragment extends MyFragment implements ApiConstants{

    public static String TAG = "VoucherFragment";
    View root;
    MyVoucherFragment myVoucherFragment;
    BuyVoucherFragment buyVoucherFragment;
    VoucherHistoryFragment voucherHistoryFragment;
    SampleFragmentPagerAdapter sampleFragmentPagerAdapter;
    ViewPager viewPagers;
    private PagerSlidingTabStrip tabStrip;
    TextView tvPoin;
    Main2Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.voucher_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        viewPagers = (ViewPager) findViewById(R.id.tabPagerVoucher);

        tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabStripVoucher);
        activity = (Main2Activity) getActivity();
        tvPoin = (TextView) findViewById(R.id.tvPoin);

        activity.refreshActionBar(getView());


        tvPoin.setText(String.valueOf(WriteReadToken.totalPoints));

        buyVoucherFragment = new BuyVoucherFragment();
        myVoucherFragment = new MyVoucherFragment();
        voucherHistoryFragment = new VoucherHistoryFragment();
        ArrayList<MyFragment> fragments = new ArrayList<>();
        fragments.add(myVoucherFragment);
        fragments.add(voucherHistoryFragment);

        String[] titles = {
                getString(R.string.yourvoucher),
                getString(R.string.lichsuvoucher),

        };
        sampleFragmentPagerAdapter = new SampleFragmentPagerAdapter(getChildFragmentManager(), fragments, titles);
        viewPagers.setAdapter(sampleFragmentPagerAdapter);
        tabStrip.setViewPager(viewPagers);

    }


    public void chageText()
    {
        tvPoin.setText(String.valueOf(WriteReadToken.totalPoints));
    }

    @Override
    public void onResume() {

        IntentFilter iff = new IntentFilter(BROADVOUCHER);
        getActivity().registerReceiver(onNotice, iff);

        super.onResume();
    }

    @Override
    public void onPause() {

        getActivity().unregisterReceiver(onNotice);
        super.onPause();
    }


    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //   checkmove = false;
            String value = intent.getExtras().getString(VALUE);

            if (value.equals(RELOADPOIN)) {


                tvPoin.setText(String.valueOf(WriteReadToken.totalPoints));
                myVoucherFragment.isReload(true);
                myVoucherFragment.reLoadVoucher();
            }


        }
    };


}