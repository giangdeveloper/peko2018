package peko.honey.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;

import peko.honey.Main2Activity;
import peko.honey.common.MyFragment;


/**
 * Created by macshop1 on 2/7/18.
 */

public class DetailNewsFragment extends MyFragment {
    public static String TAG = "DetailNewsFragment";
    View root;
    WebView webView;
    TextView tvTittle;
    String Url,Title;
    Main2Activity activity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.detailnews_fragment, container, false);
        return root;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        webView = (WebView) findViewById(R.id.wNews);
        tvTittle = (TextView)findViewById(R.id.tvTittle);
        activity = (Main2Activity) getActivity();
        activity.refreshActionBar(getView());
        Url = getArguments().getString("url");
        Title = getArguments().getString("title");


        System.out.println("url là : " + Url);

        showLoadingDialog();

        tvTittle.setText(Title);

        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);

        settings.setJavaScriptEnabled(true);
        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                 dismissLoadingDialog();

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }
        });
        webView.loadUrl(Url);

    }
}
