package peko.honey;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;

import io.fabric.sdk.android.Fabric;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;

import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyActivity;
import peko.honey.fragment.AcceptOTPFragment;
import peko.honey.fragment.ChooseSchoolFragment;
import peko.honey.fragment.InputPassFragment;
import peko.honey.fragment.InputPhoneFragment;
import peko.honey.fragment.RegisterFragment;
import peko.honey.fragment.SocialLoginFragment;
import peko.honey.fragment.VerificodeFragment;

public class LoginActivity extends MyActivity implements ApiConstants, ApiHandle.ApiCallback {

    TextView tvInputPhone, tvRegister, tvFaceBookLogin;
    CatLoadingView mView;
    InputPhoneFragment inputPhoneFragment;
    InputPassFragment inputPassFragment;
    SocialLoginFragment socialLoginFragment;
    RegisterFragment registerFragment;
    AcceptOTPFragment acceptOTPFragment;
    private CallbackManager mCallbackManager;
    String phone, token, username, email, avatar, fullName, referralCode, currentPhone;
    int id, totalPoints;
    boolean isAlive, verified;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    File f;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);

        f = new File(
                "/data/data/peko.honey/shared_prefs/CheckAccess.xml");
        if (f.exists()) {

            Intent intents = new Intent(getApplicationContext(), Main2Activity.class);
            intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            overridePendingTransition(0, 0);
            startActivity(intents);

        } else {

            tvInputPhone = (TextView) findViewById(R.id.tvInputPhone);
            tvRegister = (TextView) findViewById(R.id.tvRegister);
            tvFaceBookLogin = (TextView) findViewById(R.id.tvFaceBookLogin);

            isAlive = true;

            tvInputPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    inputPhoneFragment = new InputPhoneFragment();
                    addFragmentSlideUpAnimation(inputPhoneFragment, InputPhoneFragment.TAG, R.id.cdtLogin, true);


                    // VerificodeFragment verificodeFragment = new VerificodeFragment();
                    // addFragment(verificodeFragment,VerificodeFragment.TAG,R.id.cdtLogin,true);

                }
            });

            tvRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // registerFragment = new RegisterFragment();
                    //  addFragmentSlideUpAnimation(registerFragment, RegisterFragment.TAG, R.id.cdtLogin, true);

                    acceptOTPFragment = new AcceptOTPFragment();
                    addFragmentSlideUpAnimation(acceptOTPFragment, AcceptOTPFragment.TAG, R.id.cdtLogin, true);


                }
            });

            tvFaceBookLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    checkFbLogin();
                }
            });

        }


    }

    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //   checkmove = false;
            String value = intent.getExtras().getString(VALUE);

            if (value.equals(INPUTPHONESUCCESS)) {

                inputPassFragment = new InputPassFragment();
                Bundle bundle = new Bundle();
                bundle.putString("phone", intent.getStringExtra("phone"));
                inputPassFragment.setArguments(bundle);
                addFragment(inputPassFragment, InputPassFragment.TAG, R.id.cdtLogin, true);


            } else if (value.equals(REGISTER)) {
                registerFragment = new RegisterFragment();
                Bundle bundle = new Bundle();
                bundle.putString("phone", intent.getStringExtra("phone"));
                registerFragment.setArguments(bundle);
                addFragment(registerFragment, VerificodeFragment.TAG, R.id.cdtLogin, true);
            } else if (value.equals(CHOOSESCHOOL)) {
                ChooseSchoolFragment webViewFragment = new ChooseSchoolFragment();
                addFragment(webViewFragment, ChooseSchoolFragment.TAG, R.id.cdtLogin, false);
            }


        }
    };


    @Override
    public void onResume() {

        IntentFilter iff = new IntentFilter(BROADLOGIN);
        registerReceiver(onNotice, iff);

        super.onResume();
    }

    @Override
    public void onPause() {

        unregisterReceiver(onNotice);
        super.onPause();
    }

    void checkFbLogin() {
        if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        } else {
            loginFaceBook();
        }
    }

    //login facebook
    public void loginFaceBook() {
        // click = true;
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        if (loginResult != null) {
                            String tokenid = loginResult.getAccessToken().getCurrentAccessToken().getToken();

                            System.out.println("chay vao day khong");
                            //   fbID = loginResult.getAccessToken().getCurrentAccessToken().getUserId();
                            getLoginFB(tokenid);

                            mView = new CatLoadingView();
                            mView.setCancelable(false);
                            // mView.setCanceledOnTouchOutside(false);
                            mView.show(getSupportFragmentManager(), "");
                        }

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(), " Login Cancel ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void getLoginFB(String access) {
        ApiHandle.loginFB(API_LOGINFB + access, this, getApplicationContext());
    }

    public void refreshActionBar(View contentView) {
        TextView tvBack = (TextView) contentView.findViewById(R.id.tvBack);
        TextView tvBackSocical = (TextView) contentView.findViewById(R.id.tvBackSocical);
        TextView tvBackAccept = (TextView) contentView.findViewById(R.id.tvBackAccept);

        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    hideSoftKeyboard();
                    activity.onBackPressed();

                }
            });

        }

        if (tvBackSocical != null) {
            tvBackSocical.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backFragment(socialLoginFragment);
                }
            });
        }

        if (tvBackAccept != null) {
            tvBackAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backFragment(acceptOTPFragment);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {

        if (inputPhoneFragment != null & getBackstack() == 1) {
            backFragment(inputPhoneFragment);
        }
        if (acceptOTPFragment != null & getBackstack() == 1) {
            backFragment(acceptOTPFragment);
        }
        if (socialLoginFragment != null & getBackstack() == 1) {
            backFragment(socialLoginFragment);
        }


        super.onBackPressed();
    }

    @Override
    public void onProgress(String api) {

    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {

            mView.dismiss();

            if (result != null) {

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    if (isDoesString(jsonObject, "status")) {
                        showToast(jsonObject.getString("message"));
                    } else {

                        JSONObject jsonObjectUser = new JSONObject(jsonObject.getString("user"));

                        if (isDoesString(jsonObject, "token"))
                            token = jsonObject.getString("token");

                        if (isDoesString(jsonObjectUser, "id"))
                            id = jsonObjectUser.getInt("id");

                        if (isDoesString(jsonObjectUser, "username"))
                            username = jsonObjectUser.getString("username");

                        if (isDoesString(jsonObjectUser, "email"))
                            email = jsonObjectUser.getString("email");

                        if (isDoesString(jsonObjectUser, "phone"))
                            phone = jsonObjectUser.getString("phone");

                        if (isDoesString(jsonObjectUser, "avatar"))
                            avatar = jsonObjectUser.getString("avatar");

                        if (isDoesString(jsonObjectUser, "fullName"))
                            fullName = jsonObjectUser.getString("fullName");

                        if (isDoesString(jsonObjectUser, "verified"))
                            verified = jsonObjectUser.getBoolean("verified");


                        if (isDoesString(jsonObjectUser, "referralCode"))
                            referralCode = jsonObjectUser.getString("referralCode");

                        if (isDoesString(jsonObjectUser, "totalPoints"))
                            totalPoints = jsonObjectUser.getInt("totalPoints");

                        pref = getSharedPreferences("CheckAccess", 0);// 0 - là chế độ private
                        editor = pref.edit();
                        editor.putString("Token", token);
                        editor.putInt("id", id);
                        editor.putString("username", username);
                        editor.putInt("staticPoin", 0);
                        editor.putString("success", null);
                        editor.putString("schoolName", "");
                        editor.putString("email", email);
                        editor.putBoolean("isVerifi", verified);
                        editor.putString("phone", phone);
                        editor.putString("avatar", avatar);
                        editor.putString("fullName", fullName);
                        editor.putString("referralCode", referralCode);
                        editor.putInt("totalPoints", totalPoints);
                        editor.commit();


                        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                        intent.putExtra("isnews", 1);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getApplicationContext());
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode != RESULT_OK) {
//            return;
//        }

        if (mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }


    //check android 6.0
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
