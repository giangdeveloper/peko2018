package peko.honey.entity;

/**
 * Created by macshop1 on 11/15/17.
 */

public class RestaurentEntity {
    int id;
    int exchangerate;
    int diamond;
    String cover;
    String name;
    String location;
    int brand;
    String distance;
    String alllcation;


    public RestaurentEntity() {
    }

    public RestaurentEntity(int id, int exchangerate, int diamond, String cover, String name) {
        this.id = id;
        this.exchangerate = exchangerate;
        this.diamond = diamond;
        this.cover = cover;
        this.name = name;
    }

    public RestaurentEntity(int id, int exchangerate, int diamond, String cover, String name, String location, int brand, String distance) {

        this.id = id;
        this.exchangerate = exchangerate;
        this.diamond = diamond;
        this.cover = cover;
        this.name = name;
        this.location = location;
        this.brand = brand;
        this.distance = distance;
    }

    public RestaurentEntity(int id, int exchangerate, int diamond, String cover, String name, String location, int brand, String distance, String alllcation) {
        this.id = id;
        this.exchangerate = exchangerate;
        this.diamond = diamond;
        this.cover = cover;
        this.name = name;
        this.location = location;
        this.brand = brand;
        this.distance = distance;
        this.alllcation = alllcation;
    }

    public String getAlllcation() {
        return alllcation;
    }

    public void setAlllcation(String alllcation) {
        this.alllcation = alllcation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getExchangerate() {
        return exchangerate;
    }

    public void setExchangerate(int exchangerate) {
        this.exchangerate = exchangerate;
    }

    public int getDiamond() {
        return diamond;
    }

    public void setDiamond(int diamond) {
        this.diamond = diamond;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getBrand() {
        return brand;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
