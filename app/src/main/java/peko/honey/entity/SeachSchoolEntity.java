package peko.honey.entity;

/**
 * Created by macshop1 on 12/3/17.
 */

public class SeachSchoolEntity {
    int code;
    String name;
    String utfname;

    public SeachSchoolEntity(int code, String name, String utfname) {
        this.code = code;
        this.name = name;
        this.utfname = utfname;
    }

    public SeachSchoolEntity(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getUtfname() {
        return utfname;
    }

    public void setUtfname(String utfname) {
        this.utfname = utfname;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
