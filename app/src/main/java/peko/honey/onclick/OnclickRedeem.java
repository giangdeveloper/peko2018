package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.MyVoucherEntity;
import peko.honey.entity.RedeemEntity;

/**
 * Created by macshop1 on 11/28/17.
 */

public interface OnclickRedeem {
    void onclickRedeem(View v, RedeemEntity entity);
}
