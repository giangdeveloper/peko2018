package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.NewsEntity;
import peko.honey.entity.TrendEntity;


/**
 * Created by macshop1 on 2/7/18.
 */

public interface OnclickTrend {

    void onclickTrend(View v, TrendEntity entity);
}
