package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.VoucherHistoryEntity;

/**
 * Created by macshop1 on 12/5/17.
 */

public interface OnclickVoucherHistory {
    void onclickViewLocation(View v, VoucherHistoryEntity entity);
}
