package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.NewsEntity;


/**
 * Created by macshop1 on 2/7/18.
 */

public interface OnclickNews {

    void onclickNews(View v, NewsEntity entity);
}
