package peko.honey.onclick;

import android.view.View;

import peko.honey.entity.ExChangeGiftEntity;

/**
 * Created by macshop1 on 12/2/17.
 */

public interface OnclickExchangeGift {
    void onclickExchangeGift(View v, ExChangeGiftEntity entity);
}
