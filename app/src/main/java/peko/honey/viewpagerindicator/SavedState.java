package peko.honey.viewpagerindicator;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

/**
 * Created by Tommy on 12/28/15.
 */
public class SavedState extends View.BaseSavedState {
    int currentPage;

    public SavedState(Parcelable superState) {
        super(superState);
    }

    private SavedState(Parcel in) {
        super(in);
        currentPage = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(currentPage);
    }

    @SuppressWarnings("UnusedDeclaration")
    public final Creator<SavedState> CREATOR = new Creator<SavedState>() {
        @Override
        public SavedState createFromParcel(Parcel in) {
            return new SavedState(in);
        }

        @Override
        public SavedState[] newArray(int size) {
            return new SavedState[size];
        }
    };
}
