package peko.honey.api;

/**
 * Created by Giang on 10/13/2017.
 */

public interface ApiConstants {


    public static final String KEY_SUCCESS = "success";
    public static final String VALUE = "value";
    public static final String BROADMAIN = "BroadMain";
    public static final String BROADVOUCHER = "broadvoucher";
    public static final String RELOADMAIN = "reloadmain";
    public static final String BROADLOGIN = "BroadLogin";
    public static final String DOIQUA = "doiqua";
    public static final String DETAILRESTAURENT = "detailrestaurent";
    public static final String EXCHANGEGIFT = "exchangegift";
    public static final String INPUTPHONESUCCESS = "inputphonesuccess";
    public static final String REGISTER = "register";
    public static final String CHOOSESCHOOL = "chooseschool";
    public static final String DETAILSHOP = "detailshop";
    public static final String RELOADPOIN = "reloadpoin";

    public static final String ROOT = "https://server.pekopeko.vn";
    public static final String API_NOTIFI = ROOT + "/api/v1/users/me/notifications";
    public static final String API_PEKOSTORE = ROOT + "/api/v1/cards?page=";
    public static final String API_BUYVOUCHER = ROOT + "/api/v1.1/vouchers";
    public static final String API_VOUCHERHISTORY = ROOT + "/api/v1.1/vouchers?usable=:usable";
    public static final String API_MYVOUCHER = ROOT + "/api/v1/users/me/vouchers";
    public static final String API_DETAILSHOP = ROOT + "/api/v1/cards/";
    public static final String API_FAVORITESTORE = ROOT + "/api/v1/users/";
    public static final String SAVE_CARD = "/saved/cards?page=";
    public static final String API_GETBUYVOUCHER = ROOT + "/api/v1/users/me/vouchers/";
    public static final String BUY = "/buy";
    public static final String API_SEARCH = ROOT + "/api/v1/shops";
    public static final String API_LOGIN = ROOT + "/auth/sign_in";
    public static final String API_GETCOMMENT = ROOT + "/api/v1/shops/";
    public static final String RATES = "/rates";
    public static final String API_CHECKPHONE = ROOT + "/auth/check";
    public static final String API_VERIFI_PHONE = ROOT + "/api/v1/users/me/verification";
    public static final String API_SIGNUP = ROOT + "/auth/sign_up";
    public static final String API_PUTSHARECODE = ROOT + "/api/v1/users/me/promotion";
    public static final String API_USEVOUCHER = ROOT + "/api/v1/users/me/vouchers/";
    public static final String REDEEM = "/redeem";
    public static final String API_SAVESTORE = ROOT + "/api/v1/cards/";
    public static final String API_REDEEM = ROOT + "/api/v1/shops/";
    public static final String CARD = "/cards/";
    public static final String COLLECT = "/collect";
    public static final String API_SENDPOIN= ROOT + "/api/v1/stories";
    public static final String API_LOGINFB = ROOT + "/auth/facebook/callback?accessToken=";
    public static final String API_METADATA = ROOT + "/api/v1/metadata";
    public static final String BRANCHES =  "/branches/";
    public static final String REWARDS =  "/rewards/";
    public static final String RECEIVE =  "/receive";
    public static final String API_PHONE = "/api/v1/users/me";
    public static final String API_VERIFICODE = ROOT + "/api/v1/users/me/verification";
    public static final String API_GETSCHOOL = "https://pekocampain.000webhostapp.com/listSchoolmobile.php";
    public static final String API_NEWS = "https://public-api.wordpress.com/rest/v1.1/sites/pekopeko709358949.wordpress.com/posts";
    public static final String API_FAKEORDER = "https://thefaceshopvoucher.000webhostapp.com/orderInfo.php";
    public static final String API_FAKE1 = "https://thefaceshopvoucher.000webhostapp.com/payOrder.php";
    public static final String API_CHECKPOIN_USER = "https://pekocampain.000webhostapp.com/checkUserPoint.php";
    public static final String API_SETPOIN_SCHOOL = "https://pekocampain.000webhostapp.com/addMPoint.php";
    public static final String API_SENDSCHOOL_POIN = "https://pekocampain.000webhostapp.com/addPoint.php";
    public static final String API_GETSCHOOL_POIN = "https://pekocampain.000webhostapp.com/checkUserSchoolPoint.php";
    public static final String API_UPDATE_SCHOOL = "https://pekocampain.000webhostapp.com/updatePoint.php";
    public static final String API_TREDING = "https://public-api.wordpress.com/rest/v1.1/sites/pekocenterads.wordpress.com/posts";
    public static final String API_SLIDERES = "https://public-api.wordpress.com/rest/v1.1/sites/pekoads.wordpress.com/posts";
}
