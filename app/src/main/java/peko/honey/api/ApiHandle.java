package peko.honey.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.entity.StringEntity;
import peko.honey.readToken.WriteReadToken;
import peko.honey.util.AppConstants;
import peko.honey.util.AppUtil;


public class ApiHandle implements ApiConstants, AppConstants {

    public interface ApiCallback {
        public void onProgress(String api);

        public void onComplete(String api, String result, Object extra);
    }


    private static RequestParams createRequestParams(List<NameValuePair> nameValuePairs) {
        RequestParams params = new RequestParams();
        if (nameValuePairs != null && nameValuePairs.size() > 0) {
            for (NameValuePair pair : nameValuePairs) {
                String name = "" + pair.getName();
                String value = "" + pair.getValue();
                if (value != null && value.trim().toLowerCase().equals("null")) {
                    value = "";
                }

                params.put(name, value);

            }
        }
        return params;
    }


    private static void requestGet(final String api, final String url, final RequestParams requestParams, final ApiCallback callback, final Object extra, final Context context) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);


        final AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("X-Access-Token", WriteReadToken.Token);
        client.setConnectTimeout(2);
        client.get(url, requestParams, new TextHttpResponseHandler(UTF_8) {

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, String arg2) {
                        AppUtil.log(api + "==============onSuccess: " + arg2);
                        if (callback != null) {
                            //  client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }

                    @Override
                    public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                        AppUtil.log(api + "==============onFailure: " + arg2);
                        if (callback != null) {
                            // System.out.println("cancle request");
                            // client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }
                }
        );
    }


    private static void requestGetTest(final String api, final String url, int page,final RequestParams requestParams, final ApiCallback callback, final Object extra, final Context context) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);


        final AsyncHttpClient client = new AsyncHttpClient();

        //client.addHeader("token", "72927d8b-26cc-473b-9c5b-dfb602bf557c");
        //client.addHeader("pageidx", String.valueOf(page));
        client.setConnectTimeout(2);
        client.get(url, requestParams, new TextHttpResponseHandler(UTF_8) {

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, String arg2) {
                        AppUtil.log(api + "==============onSuccess: " + arg2);
                        if (callback != null) {
                            //  client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }

                    @Override
                    public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                        AppUtil.log(api + "==============onFailure: " + arg2);
                        if (callback != null) {
                            // System.out.println("cancle request");
                            // client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }
                }
        );
    }

    private static void requestGetFake(final String api, final String url, final RequestParams requestParams, final ApiCallback callback, final Object extra, final Context context) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);


        final AsyncHttpClient client = new AsyncHttpClient();

       // client.addHeader("X-Access-Token", WriteReadToken.Token);
        client.setConnectTimeout(2);
        client.get(url, requestParams, new TextHttpResponseHandler(UTF_8) {

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, String arg2) {
                        AppUtil.log(api + "==============onSuccess: " + arg2);
                        if (callback != null) {
                            //  client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }

                    @Override
                    public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                        AppUtil.log(api + "==============onFailure: " + arg2);
                        if (callback != null) {
                            // System.out.println("cancle request");
                            // client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }
                }
        );

    }


    private static void requestPostLogin(final Context context, final String url, final String phone, final String password, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("phone", phone);
            jsonParams.put("password", password);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

//            client.setSSLSocketFactory(
//                    new SSLSocketFactory(getSslContext(),
//                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            //  client.addHeader("Authorization", "Bearer " + WriteReadToken.Acess_Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPostCheckUser(final String url, final String userID, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);

        RequestParams params = new RequestParams();
        params.put("userID", userID);

        client.post(url, params, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }

    private static void requestPostSchoolPoin(final String url, final int userID, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);

        RequestParams params = new RequestParams();
        params.put("userID", userID);

        client.post(url, params, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }

    private static void requestPostUpdateSchool(final String url, final int userID,int code ,final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);

        RequestParams params = new RequestParams();
        params.put("userID", userID);
        params.put("schoolCode", code);

        client.post(url, params, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }


    private static void requestGetSchool(final String url, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);



        client.post(url, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }


    private static void requestPostSendSchoolPoin(final String url, final String userID ,final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);

        RequestParams params = new RequestParams();
        params.put("userID", userID);
        params.put("point", "2");

        client.post(url, params, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }

    private static void requestPostSendPoinCode(final String url, final int userID ,int code,final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        AsyncHttpClient client = new AsyncHttpClient();

        // client.addHeader("Content-Type", "multipart/form-data");

        client.setConnectTimeout(2);

        RequestParams params = new RequestParams();
        params.put("userID", userID);
        params.put("schoolCode", code);

        client.post(url, params, new TextHttpResponseHandler(UTF_8) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                AppUtil.log(url + "==============onSuccess: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                AppUtil.log(url + "==============onFailure: " + arg2);
                if (callback != null) {
                    callback.onComplete(url, arg2, extra);
                }
            }
        });


    }


    private static void requestPostUseVoucher(final Context context, final String url, final String code, final String pin, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("code", code);
            jsonParams.put("staffCode", pin);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

//            client.setSSLSocketFactory(
//                    new SSLSocketFactory(getSslContext(),
//                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPostSendPoin(final Context context, final String url, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("content", "30");

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

//            client.setSSLSocketFactory(
//                    new SSLSocketFactory(getSslContext(),
//                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPostReDeem(final Context context, final String url, final String total, final String pin, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("total", total);
            jsonParams.put("staffCode", pin);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

//            client.setSSLSocketFactory(
//                    new SSLSocketFactory(getSslContext(),
//                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private static void requestPostSearch(final Context context, final String url, final String textSearch, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("name", textSearch);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private static void requestPutShareCode(final Context context, final String url, final String code, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("promotion", code);
            jsonParams.put("type", "referralCode");
            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPost(final String api, final String url, final RequestParams requestParams, final ApiCallback callback, final Object extra, final Context context) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);

        final AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("X-Access-Token", WriteReadToken.Token);
        client.setConnectTimeout(2);
        client.post(url, requestParams, new TextHttpResponseHandler(UTF_8) {

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, String arg2) {
                        AppUtil.log(api + "==============onSuccess: " + arg2);
                        if (callback != null) {
                            //  client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }

                    @Override
                    public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                        AppUtil.log(api + "==============onFailure: " + arg2);
                        if (callback != null) {
                            // System.out.println("cancle request");
                            // client.cancelRequests(context, false);
                            callback.onComplete(api, arg2, extra);
                        }
                    }
                }
        );
    }


    private static void requestPostCheckPhone(final Context context, final String url, final String phone, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("type", "phone");
            jsonParams.put("data", phone);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private static void requestPutReWard(final Context context, final String url, final String code, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("staffCode", code);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private static void requestPostVerifiPhone(final Context context, final String url, String phone, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("phone", phone);


            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

             client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPostSignUp(final Context context, final String url, String name, final String phone, final String password, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("fullName", name);
            jsonParams.put("phone", phone);
            jsonParams.put("password", password);

            System.out.println(jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

          //  client.addHeader("X-Access-Token", WriteReadToken.Token);

            client.setConnectTimeout(2);
            client.post(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private static void requestPutCode(final Context context, final String url, String code, final RequestParams requestParams, final ApiCallback callback, final Object extra) {
        AppUtil.log("=========request: " + url);
        AppUtil.log("=========params: " + requestParams);
        AppUtil.log("=========extra: " + extra);
        try {
            JSONObject jsonParams = new JSONObject();
            jsonParams.put("code", code);


            System.out.println("json : " + jsonParams.toString());

            StringEntity strentity = new StringEntity(jsonParams.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();

//            client.setSSLSocketFactory(
//                    new SSLSocketFactory(getSslContext(),
//                            SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER));

            client.addHeader("X-Access-Token", WriteReadToken.Token);
            client.setConnectTimeout(2);
            client.put(context, url, strentity, "application/json", new TextHttpResponseHandler(UTF_8) {
                @Override
                public void onSuccess(int arg0, Header[] arg1, String arg2) {
                    AppUtil.log(url + "==============onSuccess: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }

                @Override
                public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                    AppUtil.log(url + "==============onFailure: " + arg2);
                    if (callback != null) {
                        callback.onComplete(url, arg2, extra);
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static void loginPhone(final String api, final ApiCallback callback, Context context, String phone, String pass) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostLogin(context, api, phone, pass, requestParams, callback, null);
    }

    public static void getNotifi(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }

    public static void getPekoStore(final String api, int page, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api+page);
        }
        String url = api + page;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }

    public static void getMyVoucher(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }

    public static void getMyVoucher1(final String api,int page ,final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api+page;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGetTest(url, url, page,requestParams, callback, null, context);
    }


    public static void getBuyVoucher(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }


    public static void getHistoryVoucher(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }

    public static void getExchangeGift(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, url, requestParams, callback, null, context);
    }

    public static void getFavoriteShop(final String api, int page, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api + page;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }

    public static void buyVoucher(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }

    public static void searchShop(final String api, String textSearch, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSearch(context, api, textSearch, requestParams, callback, null);
    }

    public static void putShareCode(final String api, String code, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPutShareCode(context, api, code, requestParams, callback, null);
    }


    public static void checkPhone(final String api, String phone, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostCheckPhone(context, api, phone, requestParams, callback, null);
    }

    public static void verifiPhone(final String api, String phone, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostVerifiPhone(context, url, phone, requestParams, callback, null);
    }

    public static void signUp(final String api, final ApiCallback callback, Context context, String name, String phone, String pass) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSignUp(context, api, name, phone, pass, requestParams, callback, null);
    }

    public static void useVoucher(final String api, final ApiCallback callback, Context context, String code, String pin) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostUseVoucher(context, api, code, pin, requestParams, callback, null);
    }

    public static void reeDeem(final String api, final ApiCallback callback, Context context, String total, String pin) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostReDeem(context, api, total, pin, requestParams, callback, null);
    }

    public static void saveStore(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPost(api, url, requestParams, callback, null, context);
    }

    public static void getOrderfake(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }


    public static void checkUser(final String api, String id, final ApiCallback callback) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostCheckUser( api, id, requestParams, callback, null);
    }

    public static void sendChoolPoin(final String api, String id ,final ApiCallback callback) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSendSchoolPoin( api, id,requestParams, callback, null);
    }

    public static void sendPoin(final String api,  final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSendPoin(context, url, requestParams, callback, null);
    }



    public static void sendPoinSchool(final String api, int id,int code ,final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSendPoinCode( url,id,code ,requestParams, callback, null);
    }


    public static void getPoinSchool(final String api, int id,final ApiCallback callback) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostSchoolPoin( url,id ,requestParams, callback, null);
    }

    public static void updateSchool(final String api, int id,int code,final ApiCallback callback) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPostUpdateSchool( url,id ,code,requestParams, callback, null);
    }



    public static void loginFB(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(api, api, requestParams, callback, null,context);
    }


    public static void getMetaData(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }

    public static void getReWard(final String api, String code ,final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPutReWard(context, url,code ,requestParams, callback, null);
    }

    public static void PutOTP(final String api, String code, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestPutCode(context, api, code, requestParams, callback, null);
    }

    public static void getNews(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }

    public static void getTrend(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }


    public static void getSlideRes(final String api, final ApiCallback callback, Context context) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGet(url, url, requestParams, callback, null, context);
    }

    public static void getSchool(final String api, final ApiCallback callback) {
        if (callback != null) {
            callback.onProgress(api);
        }
        String url = api;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        RequestParams requestParams = createRequestParams(nameValuePairs);
        requestGetSchool(url, requestParams, callback, null);
    }

}
