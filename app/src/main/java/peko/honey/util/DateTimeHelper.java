/**
 * 
 */
package peko.honey.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

// TODO: Auto-generated Javadoc

/**
 * The Class DateTimeHelper.
 */
public final class DateTimeHelper {

	public static final String FORMAT_FILE = "yyyyMMdd_HHmmss";
	public static final String FORMAT_FULL_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
	public static final String EMPTY_DATE = "0000-00-00";
	public static final String FORMAT_DATE = "yyyy-MM-dd";
	public static final String FORMAT_TIME = "HH:mm:ss";
	public static final String FORMAT_SHORT_TIME = "HH:mm";
	public static final String FORMAT_MESSAGE_SAME_DAY = "h:mm aa";
	public static final String FORMAT_MESSAGE_SAME_WEEK = "EEEE";
	public static final String FORMAT_MESSAGE_SAME_MONTH = "EEEE d MMM";
	public static final String FORMAT_MESSAGE_SAME_YEAR = "d MMM";
	public static final String FORMAT_DAY_TIME = "EEEE h:mm aa";
	public static final String FORMAT_LONG_DATE = "MMMM d, yyyy";
	public static final String FORMAT_MESSAGE_TIME = "EEE, MM/dd/yyyy, hh:mm aa";
	public static final String FORMAT_MATCHED_TIME = "hh:mm aa, MM/dd/yyyy";
	public static final String FORMAT_SHORT_DATE = "MMM yyyy";
	public static final String FORMAT_FB_BIRTHDAY = "MM/dd/yyyy";

	/**
	 * Clear calendar.
	 *
	 * @param calendar the calendar
	 * @return the calendar
	 */
	public static Calendar clearTimeCalendar(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	public static Calendar clearSecondCalendar(Calendar calendar) {
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * Current year.
	 *
	 * @return the int
	 */
	public static int currentYear(){
		return currentValue(Calendar.YEAR);
	}

	/**
	 * Current month.
	 *
	 * @return the int
	 */
	public static int currentMonth(){
		return currentValue(Calendar.MONTH);
	}

	/**
	 * Current day.
	 *
	 * @return the int
	 */
	public static int currentDay(){
		return currentValue(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Current value.
	 *
	 * @param field the field
	 * @return the int
	 */
	public static int currentValue(int field){
		return Calendar.getInstance().get(field);
	}
	
	public static int getAge(Calendar calendar){
		return Calendar.getInstance().get(Calendar.YEAR)
				- calendar.get(Calendar.YEAR);
	}

	/**
	 * Creat calendar.
	 *
	 * @param year the year
	 * @param month the month
	 * @param dayOfMonth the day of month
	 * @return the calendar
	 */
	public static Calendar createCalendar(int year, int month, int dayOfMonth) {
		return updateCalendar(createCalendar(), year, month, dayOfMonth);
	}

	/**
	 * Update calendar.
	 *
	 * @param calendar the calendar
	 * @param year the year
	 * @param month the month
	 * @param dayOfMonth the day of month
	 * @return the calendar
	 */
	public static Calendar updateCalendar(Calendar calendar, int year, int month, int dayOfMonth) {
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		return calendar;
	}

	/**
	 * To string.
	 *
	 * @param calendar the calendar
	 * @param format the format
	 * @return the string
	 */
	public static String toString(Calendar calendar, String format) {
		return getSimpleDateFormat(format).format(calendar.getTime());
	}

	/**
	 * To string.
	 *
	 * @param calendar the calendar
	 * @return the string
	 */
	public static String toString(Calendar calendar) {
		return toString(calendar, FORMAT_DATE);
	}

	private static SimpleDateFormat getSimpleDateFormat(String language, String format){
		Locale locale = new Locale(language);
		return new SimpleDateFormat(format, locale);
	}

	private static SimpleDateFormat getSimpleDateFormat(String format){
		Locale locale = Locale.getDefault();
		return new SimpleDateFormat(format, locale);
	}

	/**
	 * Parses the calendar.
	 *
	 * @param calendar the calendar
	 * @param format the format
	 * @return the calendar
	 */
	public static Calendar parseCalendar(String calendar, String format){
		SimpleDateFormat sdf = getSimpleDateFormat(format);
		Calendar cal = createCalendar();
		try {
			Date date = sdf.parse(calendar);
			cal.setTime(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cal;
	}
	
	public static String convert(String calendar, String fromFormat, String toFormat){
		if(calendar.equals(EMPTY_DATE)){
			return null;
		}
		Calendar cal = parseCalendar(calendar, fromFormat);
		return toString(cal, toFormat);
	}

	public static int getHour(String timeText){
		Calendar calendar = parseCalendar(timeText, FORMAT_TIME);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinute(String timeText){
		Calendar calendar = parseCalendar(timeText, FORMAT_TIME);
		return calendar.get(Calendar.MINUTE);
	}

	public static String parseTime(int hour, int minute){
		Calendar calendar = createCalendar();
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, minute);
		return toString(calendar, FORMAT_TIME);
	}

	/**
	 * Days between.
	 *
	 * @param beforeDate the before date
	 * @param afterDate the after date
	 * @return the long
	 */
	public static long daysBetween(Calendar beforeDate, Calendar afterDate) {
		Calendar date1 = (Calendar) beforeDate.clone();
		Calendar date2 = (Calendar) afterDate.clone();
		long daysBetween = 0;
		while (date1.before(date2)) {
			date1.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}

	/**
	 * Creates the calendar.
	 *
	 * @return the calendar
	 */
	public static Calendar createCalendar() {
		return Calendar.getInstance();
	}

	public static String currentCalendar(String format) {
		return toString(createCalendar(), format);
	}

	public static String currentCalendarNoSecond(String format) {
		return toString(clearSecondCalendar(createCalendar()), format);
	}

	/**
	 * Increment.
	 *
	 * @param currentDate the current date
	 */
	public static void increment(Calendar currentDate) {
		currentDate.add(Calendar.DATE, 1);
	}

	/**
	 * Checks if is last day of month.
	 *
	 * @param calendar the calendar
	 * @return true, if is last day of month
	 */
	public static boolean isLastDayOfMonth(Calendar calendar) {
		return calendar.get(Calendar.DAY_OF_MONTH) == calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * Checks if is same date.
	 *
	 * @param calendar1 the calendar1
	 * @param calendar2 the calendar2
	 * @return true, if is same date
	 */
	public static boolean isSameDate(Calendar calendar1, Calendar calendar2){
		return clearTimeCalendar((Calendar)calendar1.clone())
				.equals(clearTimeCalendar((Calendar)calendar2.clone()));
	}
	
	public static boolean isSameWeek(Calendar calendar1, Calendar calendar2){
		return calendar1.get(Calendar.WEEK_OF_YEAR) == calendar2.get(Calendar.WEEK_OF_YEAR)
				&& calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
	}

	/**
	 * Checks if is same month.
	 *
	 * @param calendar1 the calendar1
	 * @param calendar2 the calendar2
	 * @return true, if is same month
	 */
	public static boolean isSameMonth(Calendar calendar1, Calendar calendar2){
		Calendar c1 = (Calendar) calendar1.clone();
		Calendar c2 = (Calendar) calendar2.clone();
		return c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
				&& c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR);
	}
	
	public static boolean isSameYear(Calendar calendar1, Calendar calendar2){
		return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR);
	}

	public static Calendar formatDate(String timeInMillis){
		try{
			long time = Long.parseLong(timeInMillis);
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(time);
			return calendar;
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return null;
	}

	public static String formatDate(String timeInMillis, String format){
		try{
			long time = Long.parseLong(timeInMillis);
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(time);
			return toString(calendar, format);
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return "";
	}
	
	public static String formatDate(long timeInMillis, String format){
		try{
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(timeInMillis);
			return toString(calendar, format);
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		return "";
	}
}