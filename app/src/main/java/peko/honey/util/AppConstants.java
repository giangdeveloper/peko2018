package peko.honey.util;

import android.view.View;


public interface AppConstants {

	public static final String TAG = "com.tg.oio";
	public static final String CRYPTO_SEED = "synd";
	public static final String PACKAGE = "com.tg.oio";
	public static final String LINK_ON_PLAY = "https://play.google.com/store/apps/details?id=" + PACKAGE;
	
	public static final String UTF_8 = "UTF-8";

	public static final int VISIBLE = View.VISIBLE;
	public static final int GONE = View.GONE;
	public static final int INVISIBLE = View.INVISIBLE;
	public static final int CODE_ACTIVITY = 100;
	public static final int CODE_COUNTRY = 101;
	public static final int CODE_CAPTURE_IMAGE = 102;
	public static final int CODE_PICK_IMAGE = 103;
	public static final int CODE_PICK_LOCATION = 104;
	public static final int CODE_CROP_IMAGE = 105;
	public static final String FOLDER_SAVE_IMAGE = "Images";

	public static final String ACTION_REFRESH = "ACTION_REFRESH";
	public static final String KEY_CLASS_NAME = "KEY_CLASS_NAME";
	public static final String KEY_BADGE_COUNT = "KEY_BADGE_COUNT";

	public static final String KEY_TITLE = "KEY_TITLE";
	public static final String KEY_URL = "KEY_URL";
	public static final String KEY_PICK_IMAGE = "KEY_PICK_IMAGE";
	public static final String KEY_REFRESH_SCAN = "KEY_REFRESH_SCAN";
	public static final String KEY_INDEX = "KEY_INDEX";

	public static final String KEY_UID = "KEY_UID";
	public static final String KEY_JSON = "KEY_JSON";
	public static final String KEY_PASS_OBJECT = "KEY_PASS_OBJECT";
	public static final String KEY_PASS_SLIDERS = "KEY_PASS_SLIDERS";


	public static final String KEY_PROVIDER = "KEY_PROVIDER";
	public static final String KEY_PRODUCT = "KEY_PRODUCT";
	public static final String KEY_PRODUCT_ID = "KEY_PRODUCT_ID";
	public static final String KEY_CATEGORY = "KEY_CATEGORY";
	public static final String KEY_CATEGORY_ID = "KEY_CATEGORY_ID";
	public static final String KEY_BARCODE = "KEY_BARCODE";
	public static final String KEY_TYPE = "KEY_TYPE";
	public static final String KEY_SLIDE = "KEY_SLIDE";
	public static final String KEY_SETTING_NOTI = "KEY_SETTING_NOTI";
	public static final String KEY_REFRESH_ADAPTER = "KEY_REFRESH_ADAPTER";

	public static final String HISTORY_TYPE_LIKE = "HISTORY_TYPE_LIKE";
	public static final String HISTORY_TYPE_SCAN = "HISTORY_TYPE_SCAN";
	public static final String HISTORY_TYPE_BID = "HISTORY_TYPE_BID";

	public static final String EXTRA_MESSAGE = "message";
	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String[] FONTS = {
		"fonts/helveticaneue.ttf",
		"fonts/TrajanProBold.ttf",
		"fonts/TrajanProRegular.ttf"
	};
}
