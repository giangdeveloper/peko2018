package peko.honey;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.onesignal.OneSignal;

import com.peko.neko.pekoallnews.R;
import com.roger.catloadinglibrary.CatLoadingView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import peko.honey.adapter.BuyVoucherAdapter;
import peko.honey.adapter.NewsAdapter;
import peko.honey.adapter.RestaurentAdapter;
import peko.honey.adapter.SeachSchoolAdapter;
import peko.honey.adapter.SlideRestaurentAdapter;
import peko.honey.adapter.TrendAdapter;
import peko.honey.api.ApiConstants;
import peko.honey.api.ApiHandle;
import peko.honey.common.MyActivity;
import peko.honey.entity.BuyVoucherEntity;
import peko.honey.entity.NewsEntity;
import peko.honey.entity.RestaurentEntity;
import peko.honey.entity.SeachSchoolEntity;
import peko.honey.entity.SlideRestaurentEntity;
import peko.honey.entity.TrendEntity;
import peko.honey.fragment.DetailNewsFragment;
import peko.honey.fragment.DetailShopFragment;
import peko.honey.fragment.ExchangeGiftFragment;
import peko.honey.fragment.FavoriteShopFragment;
import peko.honey.fragment.NotifiFragment;
import peko.honey.fragment.ScanQRFragment;
import peko.honey.fragment.StoresFragment;
import peko.honey.fragment.VoucherFragment;
import peko.honey.fragment.WebViewFragment;
import peko.honey.onclick.OnclickBuyvoucher;
import peko.honey.onclick.OnclickNews;
import peko.honey.onclick.OnclickRestaurent;
import peko.honey.onclick.OnclickSlideRes;
import peko.honey.onclick.OnclickTrend;
import peko.honey.other.AeSimpleSHA1;
import peko.honey.readToken.WriteReadToken;
import peko.honey.view.AutoScrollViewPager;
import peko.honey.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;

public class Main2Activity extends MyActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnclickRestaurent, View.OnClickListener, ApiHandle.ApiCallback, ApiConstants, SwipeRefreshLayout.OnRefreshListener, OnclickBuyvoucher, OnclickNews, OnclickSlideRes, OnclickTrend {
    RestaurentEntity entity;
    RestaurentAdapter adapter;
    ArrayList<RestaurentEntity> results = new ArrayList<>();
    AutoCompleteTextView atSearchSchool;
    SeachSchoolAdapter seachSchoolAdapter;
    SeachSchoolEntity seachSchoolEntity;
    ArrayList<SeachSchoolEntity> resultsSearch = new ArrayList<>();
    BuyVoucherAdapter buyVoucherAdapter;
    BuyVoucherEntity buyVoucherEntity;
    ArrayList<BuyVoucherEntity> resultsBuyVoucher = new ArrayList<>();
    ShimmerRecyclerView rcBuyVoucher;
    RecyclerView rcResList;
    LinearLayoutManager mLayoutManager;
    NavigationView navigationView;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    View header;
    TextView tvScan, tvVoucher, tvNotifi, tvMenu, tvSearch;
    TextView tvNameHeader, tvMoneyHeader, tvShareCode, tvCoinHeader;
    ImageView imgAvatar;
    int PERMISSION_ALL = 1;
    int idShop, exchangeRate, giftPoints, pagePekoShop, code, currentCode, isnews, codeSchool;
    int staticPoin, currentPoin, idCurrentVoucher, idVoucher, requirePoint, totalUsingTime, status;
    String nameShop, imgShop, addressShop, location, nameSchool, currentNameVoucher, nameVoucher, nameShopVoucher;
    String coverVoucher, desVoucher, currentNameSchool, currentNameSchoolApi;
    private String sha1;
    String[] PERMISSIONS = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    boolean isAlive, isLoading, firstLoading, isRefresh, isChooseSchool;
    File f;
    Intent intent;
    public static final String ACCESS_PREFERENCES = "CheckAccess";
    WriteReadToken writeReadToken;
    private SwipeRefreshLayout swipeRefreshLayout;
    CatLoadingView mView;
    ViewPager viewpagerMember;
    ShimmerRecyclerView rcNews, rcTrend;
    String Date, Title, featured_image, URL, imageURL, link, shopName, shopAddress;
    String DateTrend, TitleTrend, featured_imageTrend, URLTrend;
    int ID, shopID, IDTrend;
    NewsEntity newEntity;
    SlideRestaurentEntity slideEntity;
    SlideRestaurentAdapter slideAdapter;
    ArrayList<SlideRestaurentEntity> resultRes = new ArrayList<>();
    NewsAdapter newsAdapter;
    ArrayList<NewsEntity> resultsNew = new ArrayList<>();
    TrendEntity trendEntity;
    TrendAdapter trendAdapter;
    ArrayList<TrendEntity> resultsTrend = new ArrayList<>();
    CirclePageIndicator circlePageIndicator;
    AutoScrollViewPager autoScrollViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvScan = (TextView) findViewById(R.id.tvScan);
        tvVoucher = (TextView) findViewById(R.id.tvVoucher);
        tvNotifi = (TextView) findViewById(R.id.tvNotifi);
        tvSearch = (TextView) findViewById(R.id.tvSearch);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header = navigationView.getHeaderView(0);
        tvNameHeader = (TextView) header.findViewById(R.id.tvNameHeader);
        tvShareCode = (TextView) header.findViewById(R.id.tvShareCode);
        tvMoneyHeader = (TextView) header.findViewById(R.id.tvMoneyHeader);
        tvCoinHeader = (TextView) findViewById(R.id.tvCoinHeader);

        imgAvatar = (ImageView) header.findViewById(R.id.imgAvatar);
        rcNews = (ShimmerRecyclerView) findViewById(R.id.rcNews);
        rcTrend = (ShimmerRecyclerView) findViewById(R.id.rcTrend);

        // rcResList = (RecyclerView) findViewById(R.id.rcResList);
        // rcBuyVoucher = (ShimmerRecyclerView) findViewById(R.id.rcBuyVoucher);
        tvMenu = (TextView) findViewById(R.id.tvMenu);

        // swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);

        //  viewpagerMember = (ViewPager) findViewById(R.id.viewpagerMember);
        circlePageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        autoScrollViewPager = (AutoScrollViewPager) findViewById(R.id.slidingPager);

        NestedScrollView mScrollView = (NestedScrollView) findViewById(R.id.scrollHome);
        mScrollView.fullScroll(ScrollView.FOCUS_UP);

        isAlive = true;

        f = new File(
                "/data/data/peko.honey/shared_prefs/CheckAccess.xml");

        if (f.exists()) {
            writeReadToken = new WriteReadToken();
            writeReadToken.getPref("CheckAccess", getApplicationContext());
        }

//        intent = getIntent();
//        Bundle extras = intent.getExtras();
//        if (extras != null) {
//            code = extras.getInt("code", 0);
//            isnews = extras.getInt("isnews", 0);
//        }

        setUpNotification();

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        tvNameHeader.setText(WriteReadToken.fullName);
        tvShareCode.setText(WriteReadToken.referralCode);
        tvCoinHeader.setText(String.valueOf(WriteReadToken.totalPoints));
        tvMoneyHeader.setText(String.valueOf(WriteReadToken.totalPoints));

        if (WriteReadToken.avatar != null & !WriteReadToken.avatar.equals("") & !WriteReadToken.avatar.isEmpty()) {
            Picasso.with(getApplicationContext())
                    .load(WriteReadToken.avatar)
                    .fit()
                    .centerCrop()
                    .into(imgAvatar);
        }

//        if (code != 0) {
//            sendPoinSchool(WriteReadToken.id, code);
//
//            SharedPreferences settings = getSharedPreferences("CheckAccess", MODE_PRIVATE);
//            SharedPreferences.Editor prefEditor = settings.edit();
//            prefEditor.putString("success", "success");
//            prefEditor.commit();
//            WriteReadToken writeReadToken = new WriteReadToken();
//            writeReadToken.getPref("CheckAccess", getApplicationContext());
//        }

//        if (isnews != 0) {
//            checkUser(String.valueOf(WriteReadToken.id));
//        }

        // getPekoStore(pagePekoShop);
        //getBuyVoucher();

//        if (writeReadToken.success == null)
//            getSchool();
//        else {
//            isChooseSchool = true;
//            getSchool();
//            getSchoolPoin();
//        }


//        if (writeReadToken.staticPoin < 3 & isnews == 0)
//            checkUser(String.valueOf(WriteReadToken.id));


        tvMenu.setOnClickListener(this);
        tvScan.setOnClickListener(this);
        tvVoucher.setOnClickListener(this);
        tvNotifi.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
        tvShareCode.setOnClickListener(this);
        tvMoneyHeader.setOnClickListener(this);
        // swipeRefreshLayout.setOnRefreshListener(this);

        getNews();
        getSlideRes();
        getTrend();

    }

    void getNews() {
        ApiHandle.getNews(API_NEWS, this, getApplicationContext());
    }

    void getTrend() {
        ApiHandle.getNews(API_TREDING, this, getApplicationContext());
    }

    void getSlideRes() {
        ApiHandle.getNews(API_SLIDERES, this, getApplicationContext());
    }

    void getPekoStore(int page) {
        ApiHandle.getPekoStore(API_PEKOSTORE, page, this, getApplicationContext());
    }

    void searchPekoShop(String textSearch) {
        ApiHandle.searchShop(API_SEARCH, textSearch, this, getApplicationContext());
    }

    void putShareCode(String code) {
        ApiHandle.putShareCode(API_PUTSHARECODE, code, this, getApplicationContext());
    }

    void sendPoinSchool(int id, int code) {
        ApiHandle.sendPoinSchool(API_SENDSCHOOL_POIN, id, code, this, getApplicationContext());
    }

    void checkUser(String id) {
        ApiHandle.checkUser(API_CHECKPOIN_USER, id, this);
    }

    void sendPoin() {
        ApiHandle.sendPoin(API_SENDPOIN, this, getApplicationContext());
    }

    void getSchool() {
        ApiHandle.getSchool(API_GETSCHOOL, this);
    }

    void getBuyVoucher() {
        ApiHandle.getBuyVoucher(API_BUYVOUCHER, this, getApplicationContext());
    }

    void buyVoucher() {
        ApiHandle.buyVoucher(API_GETBUYVOUCHER + idCurrentVoucher + BUY, this, getApplicationContext());
    }

    void getSchoolPoin() {
        ApiHandle.getPoinSchool(API_GETSCHOOL_POIN, WriteReadToken.id, this);
    }

    void updateSchool(int code) {
        ApiHandle.updateSchool(API_UPDATE_SCHOOL, WriteReadToken.id, code, this);
    }

    void resetMain() {
        rcResList = (RecyclerView) findViewById(R.id.rcResList);
        results = new ArrayList<>();
        // adapter = new RestaurentAdapter(getApplicationContext(), results, this);
        // rcResList.setAdapter(adapter);

        firstLoading = false;
        pagePekoShop = 0;
        getPekoStore(pagePekoShop);
    }

    void reLoadVoucher() {
        resultsBuyVoucher = new ArrayList<>();
        //  adapter = new MyVoucherAdapter(getActivity(), results, this);
        // rcMyVoucher.setAdapter(adapter);

        getBuyVoucher();
    }

    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //   checkmove = false;
            String value = intent.getExtras().getString(VALUE);

            if (value.equals(DOIQUA)) {

                //  buildPayMeg(getString(R.string.app_name), "Scan QR Code");
                ExchangeGiftFragment exchangeGiftFragment = new ExchangeGiftFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("diamond", intent.getIntExtra("diamond", 0));
                bundle.putString("name", intent.getStringExtra("name"));
                bundle.putInt("rate", intent.getIntExtra("rate", 0));
                bundle.putInt("id", intent.getIntExtra("id", 0));
                exchangeGiftFragment.setArguments(bundle);
                addFragment(exchangeGiftFragment, ExchangeGiftFragment.TAG, R.id.frameHome, true);


            } else if (value.equals(DETAILSHOP)) {
                DetailShopFragment detailShopFragment = new DetailShopFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("favorite", 1);
                bundle.putString("cover", intent.getStringExtra("cover"));
                bundle.putString("name", intent.getStringExtra("name"));
                bundle.putString("address", intent.getStringExtra("address"));
                bundle.putInt("brand", intent.getIntExtra("brand", 0));
                bundle.putInt("diamond", intent.getIntExtra("diamond", 0));
                bundle.putInt("id", intent.getIntExtra("id", 0));
                bundle.putInt("rate", intent.getIntExtra("rate", 0));
                detailShopFragment.setArguments(bundle);
                addFragment(detailShopFragment, DetailShopFragment.TAG, R.id.frameHome, true);
            } else if (value.equals(RELOADMAIN)) {

                resetMain();

            } else if (value.equals(DETAILRESTAURENT)) {
                DetailShopFragment detailShopFragment = new DetailShopFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("favorite", 0);
                bundle.putString("cover", intent.getStringExtra("cover"));
                bundle.putString("name", intent.getStringExtra("name"));
                bundle.putString("address", intent.getStringExtra("address"));
                bundle.putInt("brand", intent.getIntExtra("brand", 0));
                bundle.putInt("diamond", intent.getIntExtra("diamond", 0));
                bundle.putInt("id", intent.getIntExtra("id", 0));
                bundle.putInt("rate", intent.getIntExtra("rate", 0));
                detailShopFragment.setArguments(bundle);
                addFragment(detailShopFragment, DetailShopFragment.TAG, R.id.frameHome, true);
            } else if (value.equals(EXCHANGEGIFT)) {
                ExchangeGiftFragment exchangeGiftFragment = new ExchangeGiftFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("diamond", intent.getIntExtra("diamond", 0));
                bundle.putString("name", intent.getStringExtra("name"));
                bundle.putInt("rate", intent.getIntExtra("rate", 0));
                bundle.putInt("id", intent.getIntExtra("id", 0));
                exchangeGiftFragment.setArguments(bundle);
                addFragment(exchangeGiftFragment, ExchangeGiftFragment.TAG, R.id.frameHome, true);
            }


        }
    };


    @Override
    public void onResume() {

        IntentFilter iff = new IntentFilter(BROADMAIN);
        registerReceiver(onNotice, iff);

        super.onResume();
    }

    @Override
    public void onPause() {

        unregisterReceiver(onNotice);
        super.onPause();
    }


    @Override
    public void onclickRestaurent(View v, RestaurentEntity entity) {
        DetailShopFragment detailShopFragment = new DetailShopFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("favorite", 0);
        bundle.putString("cover", entity.getCover());
        bundle.putString("name", entity.getName());
        bundle.putString("address", entity.getLocation());
        bundle.putInt("brand", entity.getBrand());
        bundle.putInt("diamond", entity.getDiamond());
        bundle.putInt("id", entity.getId());
        bundle.putInt("rate", entity.getExchangerate());
        detailShopFragment.setArguments(bundle);
        addFragment(detailShopFragment, DetailShopFragment.TAG, R.id.frameHome, true);
    }

    @Override
    public void onclickExChange(View v, RestaurentEntity entity) {

        ExchangeGiftFragment exchangeGiftFragment = new ExchangeGiftFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("diamond", entity.getDiamond());
        bundle.putString("name", entity.getName());
        bundle.putInt("rate", entity.getExchangerate());
        bundle.putInt("id", entity.getId());
        exchangeGiftFragment.setArguments(bundle);
        addFragment(exchangeGiftFragment, ExchangeGiftFragment.TAG, R.id.frameHome, true);

    }

    @Override
    public void onclickViewLocation(View v, RestaurentEntity entity) {
        showDialogShowLocation(entity.getAlllcation());
    }


    @Override
    public void onclickBuyVoucher(View v, BuyVoucherEntity entity) {

        if (WriteReadToken.totalPoints >= entity.getRequirePointl()) {
            currentPoin = entity.getRequirePointl();
            currentNameVoucher = entity.getName();
            idCurrentVoucher = entity.getId();

            showDialogBuyVoucher(getResources().getString(R.string.vouchertext) + " " + entity.getName(), getResources().getString(R.string.voigia) + entity.getRequirePointl() + " " + getResources().getString(R.string.pekocoin));

        } else {
            showToast("Số điểm của bạn không đủ để mua Voucher");
        }

    }

    @Override
    public void onclickViewLocation(View v, BuyVoucherEntity entity) {
        String str = "#";
        String str1 = "\n \n";
        showDialogShowLocation(entity.getDes().replaceAll(str, str1));

    }

    public void refreshActionBar(View contentView) {
        TextView tvBack = (TextView) contentView.findViewById(R.id.tvBack);

        if (tvBack != null) {
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {

                    activity.onBackPressed();
                }
            });

        }
    }

    public void reloadPoin() {

        tvMoneyHeader.setText(String.valueOf(WriteReadToken.totalPoints));
        tvCoinHeader.setText(String.valueOf(WriteReadToken.totalPoints));
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_voucher) {

            VoucherFragment voucherFragment = new VoucherFragment();
            addFragment(voucherFragment, VoucherFragment.TAG, R.id.frameHome, true);

        } else if (id == R.id.nav_store) {
            StoresFragment storesFragment = new StoresFragment();
            addFragment(storesFragment, StoresFragment.TAG, R.id.frameHome, true);
        } else if (id == R.id.nav_favorite) {
            FavoriteShopFragment favoriteShopFragment = new FavoriteShopFragment();
            addFragment(favoriteShopFragment, FavoriteShopFragment.TAG, R.id.frameHome, true);
        } else if (id == R.id.nav_inputCode) {
            showDialogEnterVoucher();
        } else if (id == R.id.nav_logout) {
            try {
                showSignOut();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
//        else if (id == R.id.nav_december) {
//
//            WebViewFragment webViewFragment = new WebViewFragment();
//            addFragment(webViewFragment, WebViewFragment.TAG, R.id.frameHome, true);
//        } else if (id == R.id.nav_chooseSchool) {
//            // getSchool();
//            isChooseSchool = true;
//            showDialogChooseSchool();
//        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tvMenu:

                drawer.openDrawer(Gravity.LEFT);


                break;

            case R.id.tvScan:

                ScanQRFragment scanQRFragment = new ScanQRFragment();
                addFragment(scanQRFragment, ScanQRFragment.TAG, R.id.frameHome, true);

                break;

            case R.id.tvVoucher:

//                BuyVoucherFragment voucherFragment = new BuyVoucherFragment();
//                Bundle bundle = new Bundle();
//                bundle.putInt("check", 1);
//                voucherFragment.setArguments(bundle);
//                addFragment(voucherFragment, VoucherFragment.TAG, R.id.frameHome, true);

                VoucherFragment voucherFragment = new VoucherFragment();
                addFragment(voucherFragment, VoucherFragment.TAG, R.id.frameHome, true);

                break;

            case R.id.tvNotifi:

                NotifiFragment notifiFragment = new NotifiFragment();
                addFragment(notifiFragment, NotifiFragment.TAG, R.id.frameHome, true);

                break;

            case R.id.tvSearch:


                StoresFragment storesFragment = new StoresFragment();
                addFragment(storesFragment, StoresFragment.TAG, R.id.frameHome, true);

                //  showDialogSearch();

                break;

            case R.id.tvShareCode:

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, "Subject: [Peko] Tham gia ngay “Ngày thứ mấy - Trả bấy nhiêu” cùng mình nhé");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "Khuyến mãi rộn ràng, giảm giá ngập tràn theo các ngày trong tuần chỉ từ 20k - 70k với chương trình khuyến mãi mới và độc đáo của PEKO: \"NGÀY THỨ MẤY – TRẢ BẤY NHIÊU”\n" +
                        "Tham gia ngay để nhận ưu đãi:\n" +
                        "IOS: https://itunes.apple.com/vn/app/peko-peko-v%C3%AD-qu%C3%A0-t%E1%BA%B7ng/id1146328400?mt=8 \n" +
                        "Android: https://play.google.com/store/apps/details?id=peko.honey&hl=en \n" +
                        "Website: www.pekocampaign.com \n" +
                        "Mã chia sẻ : " + WriteReadToken.referralCode);
                startActivity(intent);

                break;

            case R.id.tvMoneyHeader:

                drawer.closeDrawer(GravityCompat.START);
                VoucherFragment voucherFragment1 = new VoucherFragment();
                addFragment(voucherFragment1, VoucherFragment.TAG, R.id.frameHome, true);


                break;


        }
    }


    //check android 6.0
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    public void showDialogSearch() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.search_shop_dialog);


        final EditText edSearch = (EditText) dialog.findViewById(R.id.edSearch);

        edSearch.requestFocus();

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        edSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchPekoShop(edSearch.getText().toString());
                    dialog.dismiss();

                    return true;
                }
                return false;
            }
        });

        dialog.show();


    }

    public void showDialogEnterVoucher() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.voucher_dialog);


        TextView tvCanCel = (TextView) dialog.findViewById(R.id.tvCanCel);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);
        final EditText edNhapKM = (EditText) dialog.findViewById(R.id.edNhapKM);

        tvCanCel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edNhapKM.getText().length() >= 6) {
                    dialog.dismiss();
                    putShareCode(edNhapKM.getText().toString());
                } else {
                    showToast("Mã chia sẻ không đủ");
                }
            }
        });

        dialog.show();


    }

    public void showDialogChooseSchool() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.choose_school_dialog);

        final AutoCompleteTextView atSearchSchool = (AutoCompleteTextView) dialog.findViewById(R.id.atSearchSchool);
        final LinearLayout linerSchool = (LinearLayout) dialog.findViewById(R.id.linerSchool);
        TextView tvPoinSchool = (TextView) dialog.findViewById(R.id.tvPoinSchool);
        TextView tvSchoolName = (TextView) dialog.findViewById(R.id.tvSchoolName);
        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        TextView tvAccept = (TextView) dialog.findViewById(R.id.tvAccept);

        System.out.println("name school : " + WriteReadToken.currentNameSchool);

        if (WriteReadToken.currentNameSchool != null & !WriteReadToken.currentNameSchool.equals("") & !WriteReadToken.currentNameSchool.isEmpty()) {
            linerSchool.setVisibility(View.VISIBLE);
            tvPoinSchool.setText(codeSchool + " Poin");
            tvSchoolName.setText(WriteReadToken.currentNameSchool);
        }


        seachSchoolAdapter = new SeachSchoolAdapter(this, R.layout.item_searchschool, resultsSearch);
        atSearchSchool.setAdapter(seachSchoolAdapter);

        atSearchSchool.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                currentCode = resultsSearch.get(i).getCode();
                currentNameSchool = resultsSearch.get(i).getName();
                System.out.println("code : " + currentCode);
            }
        });


        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                dialog.dismiss();
            }
        });

        atSearchSchool.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //  sendPoinSchool(WriteReadToken.id, currentCode);
                    // dialog.dismiss();
                    return true;
                }
                return false;
            }
        });

        tvSchoolName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linerSchool.setVisibility(View.GONE);
                atSearchSchool.requestFocus();
                showKeyBoard();
            }
        });

        tvAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideSoftKeyboard();

                if (WriteReadToken.currentNameSchool != null) {
                    updateSchool(currentCode);
                }

                if (currentCode != 0) {
                    sendPoinSchool(WriteReadToken.id, currentCode);

                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();


    }


    //Ham dang xuat
    public void showSignOut() throws Exception {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Bạn có muốn đăng xuất ?");

        builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                removeNotification();

                File deletePrefFile = new File("/data/data/peko.honey/shared_prefs/CheckAccess.xml");
                deletePrefFile.delete();
                WriteReadToken writeReadToken = new WriteReadToken();
                writeReadToken.staticPoin = 0;
                writeReadToken.phone = "";
                writeReadToken.avatar = "";
                writeReadToken.totalPoints = 0;
                writeReadToken.verified = false;
                writeReadToken.success = null;
                writeReadToken.email = "";
                writeReadToken.fullName = "";
                writeReadToken.id = 0;
                writeReadToken.Token = "";
                writeReadToken.referralCode = "";


                System.out.println("static poin : " + writeReadToken.staticPoin);

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(0, 0);


            }
        });

        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }


    public void setUpNotification() {
        try {
            sha1 = AeSimpleSHA1.SHA1(String.valueOf(WriteReadToken.id));
            System.out.println("sha1 send : " + sha1);
            JSONObject tags = new JSONObject();
            tags.put(sha1, true);
            OneSignal.sendTags(tags);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeNotification() {
        try {
//            sha1 = AeSimpleSHA1.SHA1(String.valueOf(WriteReadToken.id));
//            System.out.println("sha1 send : " + sha1);
//            JSONObject tags = new JSONObject();
//            tags.remove(sha1);
//            OneSignal.sendTags(tags);
            OneSignal.deleteTag(sha1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDialogBuyVoucher(String text, String Cost) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.buyvoucher_confirm_dialog);


        TextView tvTextVoucher = (TextView) dialog.findViewById(R.id.tvTextVoucher);
        TextView tvCost = (TextView) dialog.findViewById(R.id.tvCost);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        RelativeLayout relayX = (RelativeLayout) dialog.findViewById(R.id.relayX);

        tvTextVoucher.setText(Html.fromHtml(getColoredSpanned(text)));
        tvCost.setText(Html.fromHtml(getColoredSpanned(Cost)));

        relayX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                mView = new CatLoadingView();
                mView.setCancelable(false);
                // mView.setCanceledOnTouchOutside(false);
                mView.show(getSupportFragmentManager(), "");

                buyVoucher();
            }
        });

        dialog.show();


    }

    public void showDialogShowLocation(String text) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.viewlocation_dialog);

        TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
        TextView tvXacNhan = (TextView) dialog.findViewById(R.id.tvXacNhan);
        tvXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tvLocation.setText(text);

        dialog.show();
    }

    @Override
    public void onRefresh() {

        swipeRefreshLayout.setRefreshing(true);
        isRefresh = true;
        reLoadVoucher();
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        //System.out.println(s);
        return s;
    }


    @Override
    public void onProgress(String api) {
//        if (api.equals(API_PEKOSTORE + pagePekoShop) | api.equals(API_SEARCH))
//            isLoading = true;
//        //if (firstLoading == false)
//        // rcResList.showShimmerAdapter();
//
//        if (api.equals(API_BUYVOUCHER))
//            rcBuyVoucher.showShimmerAdapter();
    }

    @Override
    public void onComplete(String api, String result, Object extra) {

        if (isAlive) {

            if (api.equals(API_NEWS)) {
                if (result != null) {


                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if (isDoesString(jsonObject, "status")) {

                            removeNotification();
                            File deletePrefFile = new File("/data/data/peko.honey/shared_prefs/CheckAccess.xml");
                            deletePrefFile.delete();
                            WriteReadToken writeReadToken = new WriteReadToken();
                            writeReadToken.staticPoin = 0;
                            writeReadToken.phone = "";
                            writeReadToken.avatar = "";
                            writeReadToken.totalPoints = 0;
                            writeReadToken.verified = false;
                            writeReadToken.success = null;
                            writeReadToken.email = "";
                            writeReadToken.fullName = "";
                            writeReadToken.id = 0;
                            writeReadToken.Token = "";
                            writeReadToken.referralCode = "";

                            showToast("Token hết hạn , vui lòng đăng nhập lại");

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        } else {

                            JSONArray jsonArray = new JSONArray(jsonObject.getString("posts"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                                if (isDoesString(jsonObjectPost, "ID"))
                                    ID = jsonObjectPost.getInt("ID");

                                if (isDoesString(jsonObjectPost, "date"))
                                    Date = "Ngày tạo : " + forMatDate(jsonObjectPost.getString("date").substring(0, 19));

                                if (isDoesString(jsonObjectPost, "title"))
                                    Title = jsonObjectPost.getString("title");

                                if (isDoesString(jsonObjectPost, "URL"))
                                    URL = jsonObjectPost.getString("URL");

                                if (isDoesString(jsonObjectPost, "featured_image"))
                                    featured_image = jsonObjectPost.getString("featured_image");


                                newEntity = new NewsEntity(ID, Date, Title, URL, featured_image);
                                resultsNew.add(newEntity);

                            }

                            mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            rcNews.setLayoutManager(mLayoutManager);

                            newsAdapter = new NewsAdapter(getApplicationContext(), resultsNew, this);
                            rcNews.setAdapter(newsAdapter);


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    getNews();
                }

            } else if (api.equals(API_SLIDERES)) {
                if (result != null) {
                    try {

                        JSONObject jsonObject = new JSONObject(result);

                        JSONArray jsonArray = new JSONArray(jsonObject.getString("posts"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                            if (isDoesString(jsonObjectPost, "ID"))
                                shopID = jsonObjectPost.getInt("ID");

                            if (isDoesString(jsonObjectPost, "featured_image"))
                                imageURL = jsonObjectPost.getString("featured_image");

                            if (isDoesString(jsonObjectPost, "content"))
                                link = jsonObjectPost.getString("content").substring(12, jsonObjectPost.getString("content").length() - 14);

                            System.out.println("link : " + link);

                            if (isDoesString(jsonObjectPost, "title"))
                                shopName = String.valueOf(Html.fromHtml(jsonObjectPost.getString("title")));

                            if (isDoesString(jsonObjectPost, "title"))
                                shopAddress = jsonObjectPost.getString("title");


                            slideEntity = new SlideRestaurentEntity(shopID, imageURL, link, shopName, shopAddress);
                            resultRes.add(slideEntity);

                        }

                        slideAdapter = new SlideRestaurentAdapter(getApplicationContext(), resultRes, this, this);
                        autoScrollViewPager.setAdapter(slideAdapter);
                        autoScrollViewPager.startAutoScroll();
                        autoScrollViewPager.setInterval(8000);
                        circlePageIndicator.setViewPager(autoScrollViewPager);
                        //   viewpagerMember.setAdapter(slideAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    getSlideRes();
                }
            } else if (api.equals(API_TREDING)) {
                if (result != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        JSONArray jsonArray = new JSONArray(jsonObject.getString("posts"));
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectPost = jsonArray.getJSONObject(i);

                            if (isDoesString(jsonObjectPost, "ID"))
                                IDTrend = jsonObjectPost.getInt("ID");

                            if (isDoesString(jsonObjectPost, "date"))
                                DateTrend = "Ngày tạo : " + forMatDate(jsonObjectPost.getString("date").substring(0, 19));

                            if (isDoesString(jsonObjectPost, "title"))
                                TitleTrend = String.valueOf(Html.fromHtml(jsonObjectPost.getString("title")));

                            if (isDoesString(jsonObjectPost, "URL"))
                                URLTrend = jsonObjectPost.getString("URL");

                            if (isDoesString(jsonObjectPost, "featured_image"))
                                featured_imageTrend = jsonObjectPost.getString("featured_image");


                            trendEntity = new TrendEntity(IDTrend, DateTrend, TitleTrend, URLTrend, featured_imageTrend);
                            resultsTrend.add(trendEntity);

                        }

                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        rcTrend.setLayoutManager(mLayoutManager);

                        trendAdapter = new TrendAdapter(getApplicationContext(), resultsTrend, this);
                        rcTrend.setAdapter(trendAdapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    getTrend();
                }
            }


//            if (api.equals(API_PEKOSTORE)) {
//                if (result != null) {
//
//                    //rcResList.hideShimmerAdapter();
//                    pagePekoShop += 1;
//                    isLoading = false;
//                    if (isRefresh == true) {
//                        isRefresh = false;
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
//
//                    if (firstLoading == false) {
//
//                        firstLoading = true;
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(result);
//
//                            if(isDoesString(jsonObject,"status"))
//                            {
//
//                                removeNotification();
//
//                                File deletePrefFile = new File("/data/data/peko.honey/shared_prefs/CheckAccess.xml");
//                                deletePrefFile.delete();
//                                WriteReadToken writeReadToken = new WriteReadToken();
//                                writeReadToken.staticPoin = 0;
//                                writeReadToken.phone = "";
//                                writeReadToken.avatar = "";
//                                writeReadToken.totalPoints = 0;
//                                writeReadToken.verified = false;
//                                writeReadToken.success = null;
//                                writeReadToken.email = "";
//                                writeReadToken.fullName = "";
//                                writeReadToken.id = 0;
//                                writeReadToken.Token = "";
//                                writeReadToken.referralCode = "";
//
//                                showToast("Token hết hạn , vui lòng đăng nhập lại");
//
//                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                startActivity(intent);
//                                overridePendingTransition(0, 0);
//                            }
//                            else
//                            {
//                                JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));
//
//                                for (int i = 0; i < jsonArrayCards.length(); i++) {
//                                    JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
//                                    JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
//                                    JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
//                                    JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);
//
//                                    location = "";
//
//                                    for (int j = 0; j < jsonArrayBranches.length(); j++) {
//                                        JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(j);
//
//                                        if (isDoesString(jsonObjectBranches, "address"))
//                                            location += (jsonObjectBranches1.getString("address") + "\n \n");
//
//
//                                    }
//
//                                    if (isDoesString(jsonObjectCards, "id"))
//                                        idShop = jsonObjectCards.getInt("id");
//
//                                    if (isDoesString(jsonObjectCards, "exchangeRate"))
//                                        exchangeRate = jsonObjectCards.getInt("exchangeRate");
//
//                                    if (isDoesString(jsonObjectCards, "giftPoints"))
//                                        giftPoints = jsonObjectCards.getInt("giftPoints");
//
//                                    if (isDoesString(jsonObjectShop, "cover"))
//                                        imgShop = jsonObjectShop.getString("cover");
//
//                                    if (isDoesString(jsonObjectShop, "name"))
//                                        nameShop = jsonObjectShop.getString("name");
//
//                                    if (isDoesString(jsonObjectBranches, "address"))
//                                        addressShop = jsonObjectBranches.getString("address");
//
//                                    entity = new RestaurentEntity(idShop, exchangeRate, giftPoints, imgShop, nameShop, addressShop, jsonArrayBranches.length(), "", location);
//                                    results.add(entity);
//                                }
//
//                                mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                                mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                                rcResList.setLayoutManager(mLayoutManager);
//
//                                adapter = new RestaurentAdapter(getApplicationContext(), results, this);
//                                rcResList.setAdapter(adapter);
//
//                                rcResList.setOnScrollListener(new RecyclerView.OnScrollListener() {
//                                    @Override
//                                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                                        super.onScrolled(recyclerView, dx, dy);
//                                    }
//
//                                    @Override
//                                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                                        int totalItemCount = mLayoutManager.getItemCount();
//                                        int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
//
//                                        if (totalItemCount > 1) {
//                                            if (lastVisibleItem >= totalItemCount - 5) {
//
//                                                if (!isLoading)
//                                                    getPekoStore(pagePekoShop);
//                                            }
//                                        }
//                                    }
//                                });
//
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        try {
//
//                            System.out.println("chay den page 1");
//
//                            JSONObject jsonObject = new JSONObject(result);
//
//                            JSONArray jsonArrayCards = new JSONArray(jsonObject.getString("cards"));
//
//                            for (int i = 0; i < jsonArrayCards.length(); i++) {
//                                JSONObject jsonObjectCards = jsonArrayCards.getJSONObject(i);
//                                JSONObject jsonObjectShop = new JSONObject(jsonObjectCards.getString("shop"));
//                                JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
//                                JSONObject jsonObjectBranches = jsonArrayBranches.getJSONObject(0);
//                                location = "";
//
//                                for (int j = 0; j < jsonArrayBranches.length(); j++) {
//                                    JSONObject jsonObjectBranches1 = jsonArrayBranches.getJSONObject(j);
//
//                                    if (isDoesString(jsonObjectBranches1, "address"))
//
//                                        location += (jsonObjectBranches1.getString("address") + "\n \n");
//
//                                }
//
//
//                                if (isDoesString(jsonObjectCards, "id"))
//                                    idShop = jsonObjectCards.getInt("id");
//
//                                if (isDoesString(jsonObjectCards, "exchangeRate"))
//                                    exchangeRate = jsonObjectCards.getInt("exchangeRate");
//
//                                if (isDoesString(jsonObjectCards, "giftPoints"))
//                                    giftPoints = jsonObjectCards.getInt("giftPoints");
//
//                                if (isDoesString(jsonObjectShop, "cover"))
//                                    imgShop = jsonObjectShop.getString("cover");
//
//                                if (isDoesString(jsonObjectShop, "name"))
//                                    nameShop = jsonObjectShop.getString("name");
//
//                                if (isDoesString(jsonObjectBranches, "address"))
//                                    addressShop = jsonObjectBranches.getString("address");
//
//                                entity = new RestaurentEntity();
//                                entity.setId(idShop);
//                                entity.setExchangerate(exchangeRate);
//                                entity.setDiamond(giftPoints);
//                                entity.setCover(imgShop);
//                                entity.setAlllcation(location);
//                                entity.setName(nameShop);
//                                entity.setLocation(addressShop);
//                                entity.setBrand(jsonArrayBranches.length());
//                                results.add(entity);
//                            }
//
//                            adapter.notifyDataSetChanged();
//
//                            rcResList.setOnScrollListener(new RecyclerView.OnScrollListener() {
//                                @Override
//                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                                    super.onScrolled(recyclerView, dx, dy);
//                                }
//
//                                @Override
//                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                                    int totalItemCount = mLayoutManager.getItemCount();
//                                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
//
//                                    if (totalItemCount > 1) {
//                                        if (lastVisibleItem >= totalItemCount - 5) {
//
//                                            if (!isLoading)
//                                                getPekoStore(pagePekoShop);
//                                        }
//                                    }
//                                }
//                            });
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//
//                } else {
//
//                    getPekoStore(pagePekoShop);
//
//                }
//            } else if (api.equals(API_SEARCH)) {
//
//                if (result != null) {
//
//                    // rcResList.hideShimmerAdapter();
//                    pagePekoShop = 0;
//                    isLoading = false;
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        JSONObject jsonObjectResult = new JSONObject(jsonObject.getString("result"));
//
//                        JSONArray jsonArrayShops = new JSONArray(jsonObjectResult.getString("shops"));
//
//                        results = new ArrayList<>();
//                        adapter = new RestaurentAdapter(getApplicationContext(), results, this);
//                        rcResList.setAdapter(adapter);
//
//                        for (int i = 0; i < jsonArrayShops.length(); i++) {
//                            JSONObject jsonObjectShop = jsonArrayShops.getJSONObject(i);
//
//                            JSONArray jsonArrayBranches = new JSONArray(jsonObjectShop.getString("branches"));
//
//                            JSONObject jsonObjectBrand = jsonArrayBranches.getJSONObject(0);
//
//                            if (isDoesString(jsonObjectShop, "id"))
//                                idShop = jsonObjectShop.getInt("id");
//
////                            if (isDoesString(jsonObjectCards, "exchangeRate"))
////                                exchangeRate = jsonObjectCards.getInt("exchangeRate");
////
////                            if (isDoesString(jsonObjectCards, "giftPoints"))
////                                giftPoints = jsonObjectCards.getInt("giftPoints");
//
//                            if (isDoesString(jsonObjectShop, "cover"))
//                                imgShop = jsonObjectShop.getString("cover");
//
//                            if (isDoesString(jsonObjectShop, "name"))
//                                nameShop = jsonObjectShop.getString("name");
//
//                            if (isDoesString(jsonObjectBrand, "address"))
//                                addressShop = jsonObjectBrand.getString("address");
//
//                            entity = new RestaurentEntity(idShop, 0, 0, imgShop, nameShop, addressShop, jsonArrayBranches.length(), "");
//                            results.add(entity);
//
//                        }
//
//                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                        rcResList.setLayoutManager(mLayoutManager);
//
//                        adapter = new RestaurentAdapter(getApplicationContext(), results, this);
//                        rcResList.setAdapter(adapter);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//
//                }
//            } else if (api.equals(API_PUTSHARECODE)) {
//                if (result != null) {
//
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        if (isDoesString(jsonObject, "status")) {
//                            if (jsonObject.getInt("status") == 404) {
//                                showDialogMess(jsonObject.getString("message"), this);
//                            } else if (jsonObject.getInt("status") == 200) {
//                                showDialogMess(getResources().getString(R.string.nhapmathanhcong), this);
//                            }
//                        }
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else if (api.equals(API_CHECKPOIN_USER)) {
//                if (result != null) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        if (isDoesString(jsonObject, "id")) {
//                            int id = jsonObject.getInt("id");
//
//                            if (id == -1) {
//                                sendPoin();
//                            } else {
//                                SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                                SharedPreferences.Editor prefEditor = settings.edit();
//                                prefEditor.putInt("staticPoin", 3);
//                                prefEditor.commit();
//                            }
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    checkUser(String.valueOf(WriteReadToken.id));
//                }
//            } else if (api.equals(API_SENDPOIN)) {
//                if (result != null) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        staticPoin += 1;
//
//                        if (staticPoin <= 2)
//                            sendPoin();
//
//                        SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                        SharedPreferences.Editor prefEditor = settings.edit();
//                        prefEditor.putInt("staticPoin", staticPoin);
//                        prefEditor.putInt("totalPoints", WriteReadToken.totalPoints + 30);
//                        prefEditor.commit();
//
//                        WriteReadToken writeReadToken = new WriteReadToken();
//                        writeReadToken.getPref("CheckAccess", getApplicationContext());
//
//                        tvMoneyHeader.setText(String.valueOf(WriteReadToken.totalPoints) + " Coin");
//                        tvCoinHeader.setText(String.valueOf(WriteReadToken.totalPoints));
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    if (staticPoin < 3)
//                        sendPoin();
//                }
//            } else if (api.equals(API_SENDSCHOOL_POIN)) {
//                if (result != null) {
//
//
//                    if (WriteReadToken.success == null) {
//                        getSchool();
//
//                        SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                        SharedPreferences.Editor prefEditor = settings.edit();
//                        prefEditor.putString("success", "success");
//                        prefEditor.commit();
//                        WriteReadToken writeReadToken = new WriteReadToken();
//                        writeReadToken.getPref("CheckAccess", getApplicationContext());
//                    }
//
//
//                } else {
//                    sendPoinSchool(WriteReadToken.id, code);
//                }
//            } else if (api.equals(API_GETSCHOOL)) {
//                if (result != null) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//                        JSONArray jsonArraySchools = new JSONArray(jsonObject.getString("schools"));
//                        for (int i = 0; i < jsonArraySchools.length(); i++) {
//                            JSONObject jsonObjectSchool = jsonArraySchools.getJSONObject(i);
//
//                            if (isDoesString(jsonObjectSchool, "code"))
//                                code = jsonObjectSchool.getInt("code");
//
//                            if (isDoesString(jsonObjectSchool, "name"))
//                                nameSchool = jsonObjectSchool.getString("name");
//
//                            seachSchoolEntity = new SeachSchoolEntity(code, nameSchool, stripAccents(nameSchool));
//                            resultsSearch.add(seachSchoolEntity);
//
//                        }
//
//                        if (isChooseSchool == false)
//                            showDialogChooseSchool();
//
//                        if (WriteReadToken.success == null) {
//                            SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                            SharedPreferences.Editor prefEditor = settings.edit();
//                            prefEditor.putString("success", "success");
//                            prefEditor.commit();
//                            WriteReadToken writeReadToken = new WriteReadToken();
//                            writeReadToken.getPref("CheckAccess", getApplicationContext());
//                        }
//
//                        isChooseSchool = false;
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    getSchool();
//                }
//            } else if (api.equals(API_BUYVOUCHER)) {
//                if (result != null) {
//
//                    rcBuyVoucher.hideShimmerAdapter();
//
//                    if (isRefresh == true) {
//                        isRefresh = false;
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
//
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        JSONArray jsonArrayVoucher = new JSONArray(jsonObject.getString("vouchers"));
//                        for (int i = 0; i < jsonArrayVoucher.length(); i++) {
//                            JSONObject jsonObjectVoucher = jsonArrayVoucher.getJSONObject(i);
//
//
//                            JSONObject jsonObjectShop = new JSONObject(jsonObjectVoucher.getString("shop"));
//
//                            if (isDoesString(jsonObjectVoucher, "id"))
//                                idVoucher = jsonObjectVoucher.getInt("id");
//
//                            if (isDoesString(jsonObjectVoucher, "requirePoint"))
//                                requirePoint = jsonObjectVoucher.getInt("requirePoint");
//
//                            if (isDoesString(jsonObjectVoucher, "totalUsingTime"))
//                                totalUsingTime = jsonObjectVoucher.getInt("totalUsingTime");
//
//                            if (isDoesString(jsonObjectShop, "name"))
//                                nameShopVoucher = jsonObjectShop.getString("name");
//
//                            if (isDoesString(jsonObjectVoucher, "name"))
//                                nameVoucher = jsonObjectVoucher.getString("name");
//
//                            if (isDoesString(jsonObjectVoucher, "media"))
//                                coverVoucher = jsonObjectVoucher.getString("media");
//
//                            if (isDoesString(jsonObjectVoucher, "description"))
//                                desVoucher = jsonObjectVoucher.getString("description");
//
//
//                            buyVoucherEntity = new BuyVoucherEntity(idVoucher, nameVoucher, nameShopVoucher, desVoucher, coverVoucher, requirePoint, totalUsingTime);
//                            resultsBuyVoucher.add(buyVoucherEntity);
//                        }
//
//                        mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                        rcBuyVoucher.setLayoutManager(mLayoutManager);
//
//                        buyVoucherAdapter = new BuyVoucherAdapter(getApplicationContext(), resultsBuyVoucher, this);
//                        rcBuyVoucher.setAdapter(buyVoucherAdapter);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    rcBuyVoucher.hideShimmerAdapter();
//                    getBuyVoucher();
//                }
//            } else if (api.equals(API_GETBUYVOUCHER + idCurrentVoucher + BUY)) {
//
//
//                mView.dismiss();
//
//                if (result != null) {
//
//
//                    try {
//                        JSONObject jsonObject = new JSONObject(result);
//
//                        if (isDoesString(jsonObject, "status"))
//                            status = jsonObject.getInt("status");
//
//                        if (status == 200) {
//
//                            // showDialogMess("Bạn đã mua voucher " + currentNameVoucher + " thành công", getActivity());
//                            SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                            SharedPreferences.Editor prefEditor = settings.edit();
//                            prefEditor.putInt("totalPoints", WriteReadToken.totalPoints - currentPoin);
//                            prefEditor.commit();
//
//                            WriteReadToken writeReadToken = new WriteReadToken();
//                            writeReadToken.getPref("CheckAccess", getApplicationContext());
//
//                            reloadPoin();
//
//                            // sendBoardCast(BROADVOUCHER, RELOADPOIN);
//
//                        } else {
//                            showDialogMess(getResources().getString(R.string.coloitrongquatrinh), getApplicationContext());
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//
//                }
//            } else if (api.equals(API_GETSCHOOL_POIN)) {
//                if (result != null) {
//
//                    if (result.equals("0")) {
//
//                    } else {
//                        try {
//
//
//                            JSONObject jsonObject = new JSONObject(result);
//
//                            if (isDoesString(jsonObject, "code"))
//                                codeSchool = jsonObject.getInt("code");
//
//                            if (isDoesString(jsonObject, "name"))
//                                currentNameSchoolApi = jsonObject.getString("name");
//
//                            if (WriteReadToken.currentNameSchool == null | WriteReadToken.currentNameSchool.equals("") | WriteReadToken.currentNameSchool.isEmpty()) {
//                                SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                                SharedPreferences.Editor prefEditor = settings.edit();
//                                prefEditor.putString("schoolName", currentNameSchoolApi);
//                                prefEditor.commit();
//
//                                WriteReadToken writeReadToken = new WriteReadToken();
//                                writeReadToken.getPref("CheckAccess", getApplicationContext());
//
//                            } else if (currentNameSchoolApi.equals(WriteReadToken.currentNameSchool)) {
//                                SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                                SharedPreferences.Editor prefEditor = settings.edit();
//                                prefEditor.putString("schoolName", currentNameSchoolApi);
//                                prefEditor.commit();
//
//                                WriteReadToken writeReadToken = new WriteReadToken();
//                                writeReadToken.getPref("CheckAccess", getApplicationContext());
//                            }
//
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//
//            } else if (api.equals(API_UPDATE_SCHOOL)) {
//                if (result != null) {
//                    SharedPreferences settings = getSharedPreferences(ACCESS_PREFERENCES, MODE_PRIVATE);
//                    SharedPreferences.Editor prefEditor = settings.edit();
//                    prefEditor.putString("schoolName", currentNameSchool);
//                    prefEditor.commit();
//
//                    WriteReadToken writeReadToken = new WriteReadToken();
//                    writeReadToken.getPref("CheckAccess", getApplicationContext());
//
//                    showDialogMess("Bạn đã thay đổi trường thành công", this);
//                } else {
//                    showDialogMess(getResources().getString(R.string.coloitrongquatrinh), this);
//                }
//            }
        }

    }

    @Override
    public void onclickNews(View v, NewsEntity entity) {
        DetailNewsFragment detailNewsFragment = new DetailNewsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", entity.getUrl());
        bundle.putString("title", entity.getTitle());
        detailNewsFragment.setArguments(bundle);
        addFragment(detailNewsFragment, DetailNewsFragment.TAG, R.id.frameHome, true);
    }

    @Override
    public void onclickSlide(View v, SlideRestaurentEntity entity) {
        DetailNewsFragment detailNewsFragment = new DetailNewsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", entity.getUrl());
        bundle.putString("title", entity.getShopname());
        detailNewsFragment.setArguments(bundle);
        addFragment(detailNewsFragment, DetailNewsFragment.TAG, R.id.frameHome, true);
    }

    @Override
    public void onclickTrend(View v, TrendEntity entity) {
        DetailNewsFragment detailNewsFragment = new DetailNewsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("url", entity.getUrl());
        bundle.putString("title", entity.getTitle());
        detailNewsFragment.setArguments(bundle);
        addFragment(detailNewsFragment, DetailNewsFragment.TAG, R.id.frameHome, true);
    }
}
