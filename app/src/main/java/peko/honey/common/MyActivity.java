package peko.honey.common;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.peko.neko.pekoallnews.R;

import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public abstract class MyActivity extends FragmentActivity {


    public MyActivity activity;
    public FragmentManager fragmentManager;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        fragmentManager = getSupportFragmentManager();
    }


    protected void onCreate(Bundle savedInstanceState, int layoutResID) {
        super.onCreate(savedInstanceState);
        activity = this;
        fragmentManager = getSupportFragmentManager();
        if (layoutResID != 0) {
            setContentView(layoutResID);
        }
    }

    public void showDialogMess(String mess,Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.sucees_dialog);


        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);

        tvTitle.setText(mess);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void hideSoftKeyboard() {

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void showKeyBoard()
    {
        InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public String getColoredSpanned(String text) {
        String input = "<font color=" + "#000000" + ">" + text + "</font>";
        return input;
    }

    public void sendBoardCast(String board, String value) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        sendBroadcast(i);

    }

    public void sendBoardCastExchange(String board, String value,int giftPoin,int exchangeRate) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        i.putExtra("giftPoin", giftPoin);
        i.putExtra("rate", exchangeRate);
        sendBroadcast(i);

    }


    public boolean isDoesString(JSONObject jsonObject, String str) {
        if (jsonObject.has(str))
            return true;

        return false;
    }

    public void showToast(String mess) {
        Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_SHORT).show();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }

    public String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getDateTimeForMat() {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    public String forMatDate(String strDate) {
        // 2018-02-05T03:06:37+00:00
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String newFormat = formatter.format(date);

        System.out.println("create at : " + newFormat.toString());

        return newFormat.toString();
    }

//
//    public void changeActionBarWithMenu(int layoutId, int stringId) {
//        changeActionBar(layoutId, getString(stringId));
//    }
//
//    public void changeActionBarWithMenu(int layoutId, String title) {
//        changeActionBar(layoutId, title);
//    }

//    private void changeActionBar(int layoutId, String title) {
//        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getActionBar().setCustomView(layoutId);
//        View titleText = getActionBarView(R.id.textTitle);
//        if (titleText != null) {
//            ((TextView) titleText).setText(null);
//            if (title != null) {
//                ((TextView) titleText).setText(title.toUpperCase());
//            }
//            titleText.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
//                }
//            });
//        }
//
//    }
//
//    public void changeActionBarTitle(int stringId) {
//        changeActionBarTitle(getString(stringId));
//    }
//
//    public void changeActionBarTitle(String title) {
//        View titleText = getActionBarView(R.id.textTitle);
//        if (titleText != null) {
//            ((TextView) titleText).setText(null);
//            if (title != null) {
//                ((TextView) titleText).setText(title.toUpperCase());
//            }
//        }
//    }

//    public View getActionBarView(int id) {
//        return getActionBar().getCustomView().findViewById(id);
//    }


    public void removeFragments() {
        try {
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void replaceFragment(Fragment fragment, String tag, int frameId, boolean addToBackStack) {
        try {
            if (addToBackStack) {
                fragmentManager.beginTransaction()
                        .replace(frameId, fragment, tag)
                        .addToBackStack(tag)
                        .commitAllowingStateLoss();
            } else {
                fragmentManager.beginTransaction()
                        .replace(frameId, fragment, tag)
                        .commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFragment(Fragment fragment, String tag, int frameId, boolean addToBackStack) {
        try {
            if (addToBackStack) {
                fragmentManager.beginTransaction()
                        // .setCustomAnimations(R.anim.pull_right, R.anim.pull_left)
                        .add(frameId, fragment, tag)
                        .addToBackStack(tag)
                        .commitAllowingStateLoss();
            } else {
                fragmentManager.beginTransaction()
                        //.setCustomAnimations(R.anim.pull_right, R.anim.pull_left)
                        .add(frameId, fragment, tag)
                        .commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addFragmentSlideUpAnimation(Fragment fragment, String tag, int frameId, boolean addToBackStack) {
        try {
            if (addToBackStack) {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
                        .add(frameId, fragment, tag)
                        .addToBackStack(tag)
                        .commitAllowingStateLoss();
            } else {
                fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
                        .add(frameId, fragment, tag)
                        .commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public int getBackstack()
    {
       return fragmentManager.getBackStackEntryCount();
    }


    public void backFragment(Fragment fragment)
    {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_out_down);
        ft.remove(fragment);
        ft.commit();
        //ft.popBackStack();
    }

    public boolean isNull(String x) {
        if (x != null & !x.equals("null") & !x.isEmpty())
            return true;

        return false;
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Check if there is fast connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnectedFast(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(), info.getSubtype()));
    }

    /**
     * Check if the connection is fast
     *
     * @param type
     * @param subType
     * @return
     */
    public static boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    System.out.println("1");
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    System.out.println("2");
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    System.out.println("3");
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    System.out.println("4");
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    System.out.println("5");
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    System.out.println("6");
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    System.out.println("7");
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    System.out.println("8");
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    System.out.println("9");
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    System.out.println("10");
                    return true; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these
			 */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    System.out.println("11");
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    System.out.println("12");
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    System.out.println("13");
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    System.out.println("14");
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    System.out.println("15");
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

}
