package peko.honey.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;


import com.peko.neko.pekoallnews.R;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by GT on 1/15/2016.
 */
public class MyFragment extends Fragment {
    protected FragmentManager childFragmentManager;
    protected LayoutInflater inflater;

    public MyFragment() {
        super();
        setArguments(new Bundle());
    }

    public void addChildFragment(Fragment fragment, String tag, int frameId, boolean addToBackStack) {
        try {
            if (addToBackStack) {
                childFragmentManager.beginTransaction()

                        .add(frameId, fragment, tag)
                        .addToBackStack(tag)
                        .commitAllowingStateLoss();
            } else {
                childFragmentManager.beginTransaction()

                        .add(frameId, fragment, tag)
                        .commitAllowingStateLoss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDialogMess(String mess,Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.sucees_dialog);


        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);

        tvTitle.setText(mess);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void showDialogVoucher(String mess,String code,Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.sucees_dialog_voucher);


        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvCode = (TextView)dialog.findViewById(R.id.tvCode);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);

        tvTitle.setText(mess);
        tvCode.setText(code);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void showLoadingDialog() {
        try {
            findViewById(R.id.loadingDialog).setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissLoadingDialog() {
        try {
            findViewById(R.id.loadingDialog).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void sendBoardCast(String board, String value) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        getActivity().sendBroadcast(i);
        hideSoftKeyboard(getActivity());
    }

    public String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public String getColoredSpanned(String text) {
        String input = "<font color=" + "#000000" + ">" + text + "</font>";
        return input;
    }

    public void sendBoardCastExchageGift(String board, String value,int diamond, String name,int rate,int id) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        i.putExtra("diamond",diamond);
        i.putExtra("name", name);
        i.putExtra("rate", rate);
        i.putExtra("id", id);
        getActivity().sendBroadcast(i);
        hideSoftKeyboard(getActivity());
    }




    public void sendBoardCastDetailShop(String board, String value,int diamond, String name,int rate,int id,String cover,String address,int brand) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        i.putExtra("cover", cover);
        i.putExtra("name", name);
        i.putExtra("address", address);
        i.putExtra("brand", brand);
        i.putExtra("diamond", diamond);
        i.putExtra("id", id);
        i.putExtra("rate", rate);
        getActivity().sendBroadcast(i);
        hideSoftKeyboard(getActivity());
    }


    public void sendBoardCast(String board, String value,String phone) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        i.putExtra("phone", phone);
        getActivity().sendBroadcast(i);
        hideSoftKeyboard(getActivity());
    }


    public void sendBoardCastRestaurent(String board, String value,String cover,String name,String address,int brand,int diamond,int id,int rate) {
        Intent i = new Intent();
        i.setAction(board);
        i.putExtra("value", value);
        i.putExtra("favorite", 0);
        i.putExtra("cover", cover);
        i.putExtra("name", name);
        i.putExtra("address", address);
        i.putExtra("brand",brand);
        i.putExtra("diamond", diamond);
        i.putExtra("id", id);
        i.putExtra("rate", rate);
        getActivity().sendBroadcast(i);
        hideSoftKeyboard(getActivity());
    }



    public void hideSoftKeyboard(Activity activity) {

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public boolean isDoesString(JSONObject jsonObject, String str) {
        if (jsonObject.has(str))
            return true;

        return false;
    }



    public void removeCurrentFragment(Fragment fragment)
    {
        getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }


    public Bitmap getBitmapFromUri(Uri contentUri,Context context) {
        String path = null;
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            path = cursor.getString(columnIndex);
        }
        cursor.close();
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        return bitmap;
    }

    public boolean isNull(String x) {
        if (x != null & !x.equals("null") & !x.isEmpty())
            return true;

        return false;
    }

    public void showToast(String mess) {
        Toast.makeText(getActivity(), mess, Toast.LENGTH_SHORT).show();
    }

    public void showToastLong(String mess) {
        Toast.makeText(getActivity(), mess, Toast.LENGTH_LONG).show();
    }

    public void showToastKetNoi() {
        Toast.makeText(getActivity(), "Kết nối không ổn định , vui lòng thử lại", Toast.LENGTH_LONG).show();
    }

    public String decode(String url) {
        return url.replace("&amp;", "&");
    }

    public String decodePhay(String url) {
        return url.replace("&#x27;", "'");
    }


    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getDateMonth() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String parseDateToddMMyyyyy(String time) {
        String inputPattern = "yyyy-MM-dd'T'HH:mm:ss.sss'Z'";
        String outputPattern = "HH:mm dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String forMatDate(String strDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.sss'Z'");
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy ");
        String newFormat = formatter.format(date);

        System.out.println("create at : " + newFormat.toString());

        return newFormat.toString();
    }


//    public void showError() {
//        findViewById(R.id.linerError).setVisibility(View.VISIBLE);
//    }
//
//    public void hideError() {
//        findViewById(R.id.linerError).setVisibility(View.GONE);
//    }
//
//
//    public void showLoadingDialog() {
//        try {
//            findViewById(R.id.loadingDialog).setVisibility(View.VISIBLE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void dismissLoadingDialog() {
//        try {
//            findViewById(R.id.loadingDialog).setVisibility(View.GONE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public void clearChildFragments() {
        try {
            childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showKeyBoard()
    {
        InputMethodManager inputMethodManager=(InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.childFragmentManager = getChildFragmentManager();
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }


    public View createView(LayoutInflater inflater, ViewGroup container, Bundle state, int layoutId) {
        return inflater.inflate(layoutId, container, false);
    }

    public View findViewById(int id) {
        return getView().findViewById(id);
    }

    public boolean canBack() {
        return childFragmentManager.popBackStackImmediate();
    }

//    public void dismissLoadingDialog() {
//        try {
//            findViewById(R.id.loadingDialog).setVisibility(View.GONE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void showLoadingDialog() {
//        try {
//            findViewById(R.id.loadingDialog).setVisibility(View.VISIBLE);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
