package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.RedeemEntity;
import peko.honey.entity.RestaurentEntity;
import peko.honey.onclick.OnclickRedeem;
import peko.honey.onclick.OnclickRestaurent;


/**
 * Created by Giang on 7/4/2017.
 */

public class RedeemAdapter extends RecyclerView.Adapter<RedeemAdapter.ViewHolder> {

    ArrayList<RedeemEntity> arrayList;
    Context context;
    OnclickRedeem onclickRedeem;

    public static class ViewHolder extends RecyclerView.ViewHolder {
       private final TextView tvName,tvPin;


        public ViewHolder(View v) {
            super(v);

            tvName = (TextView)v.findViewById(R.id.tvName);
            tvPin = (TextView)v.findViewById(R.id.tvPin);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public RedeemAdapter(Context context, ArrayList<RedeemEntity> arrayList,OnclickRedeem onclickRedeem) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickRedeem = onclickRedeem;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_redeem, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final RedeemEntity curentPost = arrayList.get(position);


        viewHolder.tvName.setText(curentPost.getAddress());

        viewHolder.tvPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickRedeem.onclickRedeem(view,curentPost);
            }
        });


      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
