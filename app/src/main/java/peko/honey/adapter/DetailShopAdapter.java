package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.peko.neko.pekoallnews.R;
import peko.honey.entity.DetailShopEntity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Giang on 7/4/2017.
 */

public class DetailShopAdapter extends RecyclerView.Adapter<DetailShopAdapter.ViewHolder> {

    ArrayList<DetailShopEntity> arrayList;
    Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
       // private final TextView tvName,tvTien;
      //  private final LinearLayout linerMonDat;
      //  private final ImageView imgCoverRes;

        public ViewHolder(View v) {
            super(v);
          //  tvName = (TextView) v.findViewById(R.id.tvTenMonChon);
          //  tvTien = (TextView)v.findViewById(R.id.tvTien);
          //  linerMonDat = (LinearLayout) v.findViewById(R.id.linerMonDat);
         //   imgCoverRes = (ImageView)v.findViewById(R.id.imgCoverRes);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public DetailShopAdapter(Context context, ArrayList<DetailShopEntity> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_detailshop, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final DetailShopEntity curentPost = arrayList.get(position);


//        viewHolder.imgCoverRes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//            }
//        });

      //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());




    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
