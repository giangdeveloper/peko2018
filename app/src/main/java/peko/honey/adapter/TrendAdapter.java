package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.NewsEntity;
import peko.honey.entity.TrendEntity;
import peko.honey.onclick.OnclickNews;
import peko.honey.onclick.OnclickTrend;


/**
 * Created by Giang on 7/4/2017.
 */

public class TrendAdapter extends RecyclerView.Adapter<TrendAdapter.ViewHolder> {

    ArrayList<TrendEntity> arrayList;
    Context context;
    OnclickTrend onclickTrend;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitleTrend;
        private final ImageView imgTrend;

        public ViewHolder(View v) {
            super(v);
            tvTitleTrend = (TextView) v.findViewById(R.id.tvTitleTrend);
            imgTrend = (ImageView) v.findViewById(R.id.imgTrend);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public TrendAdapter(Context context, ArrayList<TrendEntity> arrayList, OnclickTrend onclickTrend) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickTrend = onclickTrend;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_trend, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final TrendEntity curentPost = arrayList.get(position);

        viewHolder.tvTitleTrend.setText(Html.fromHtml(curentPost.getTitle()));


        if (curentPost.getImage() != null & !curentPost.getImage().equals("") & !curentPost.getImage().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getImage())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgTrend);
        }

        viewHolder.imgTrend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickTrend.onclickTrend(view,curentPost);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }

    public String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}
