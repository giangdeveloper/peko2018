package peko.honey.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.SlideRestaurentEntity;
import peko.honey.onclick.OnclickSlideRes;


/**
 * Created by macshop1 on 12/14/17.
 */

public class SlideRestaurentAdapter extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflater;
    Activity activity = (Activity) context;
    ArrayList<SlideRestaurentEntity> arrayList;
    OnclickSlideRes onclickSlideRes;

    public SlideRestaurentAdapter(Context context, ArrayList<SlideRestaurentEntity> arrayList, Activity activity,OnclickSlideRes onclickSlideRes) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrayList = arrayList;
        this.activity = activity;
        this.onclickSlideRes = onclickSlideRes;
    }

    @Override
    public int getCount() {
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }
    @Override
    public float getPageWidth (int position) {
       // if(arrayList.size()==1){
            return 1f;
       // }
//        return 0.87f;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.item_slide_res, container, false);

        ImageView imgBg = (ImageView) itemView.findViewById(R.id.imgBg);


        final SlideRestaurentEntity curentPost = arrayList.get(position);

        if (curentPost.getImage() != null & !curentPost.getImage().equals("") & !curentPost.getImage().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getImage())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(imgBg);
        }

        imgBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickSlideRes.onclickSlide(view,curentPost);
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }
}
