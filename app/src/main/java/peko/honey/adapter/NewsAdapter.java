package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import peko.honey.entity.NewsEntity;
import peko.honey.onclick.OnclickNews;


/**
 * Created by Giang on 7/4/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    ArrayList<NewsEntity> arrayList;
    Context context;
    OnclickNews onclickNews;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName, tvDate;
        private final LinearLayout linerNews;
        private final ImageView imgBg;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            linerNews = (LinearLayout) v.findViewById(R.id.linerNews);
            imgBg = (ImageView) v.findViewById(R.id.imgBg);
        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public NewsAdapter(Context context, ArrayList<NewsEntity> arrayList, OnclickNews onclickNews) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickNews = onclickNews;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_news, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final NewsEntity curentPost = arrayList.get(position);

        viewHolder.tvName.setText(Html.fromHtml(curentPost.getTitle()));
        viewHolder.tvDate.setText(curentPost.getDate());

        viewHolder.linerNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickNews.onclickNews(view, curentPost);
            }
        });

        if (curentPost.getImage() != null & !curentPost.getImage().equals("") & !curentPost.getImage().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getImage())
                    .fit()
                    .centerCrop()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgBg);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }

    public String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

}
