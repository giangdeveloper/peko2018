package peko.honey.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.peko.neko.pekoallnews.R;
import com.squareup.picasso.Picasso;

import peko.honey.entity.MyVoucherEntity;
import peko.honey.onclick.OnclickUsevoucher;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


/**
 * Created by Giang on 7/4/2017.
 */

public class MyVoucherAdapter extends RecyclerView.Adapter<MyVoucherAdapter.ViewHolder> {

    ArrayList<MyVoucherEntity> arrayList;
    Context context;
    OnclickUsevoucher onclickUsevoucher;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvPoin, tvBuy, tvNumber, tvName, tvDes;
        private final ImageView imgCover, imgLocation;
        private final LinearLayout linerBuyVoucher;
        private final RelativeLayout relayLocation;
        //  private final ImageView imgCoverRes;

        public ViewHolder(View v) {
            super(v);
            tvPoin = (TextView) v.findViewById(R.id.tvPoin);
            tvBuy = (TextView) v.findViewById(R.id.tvBuy);
            tvNumber = (TextView) v.findViewById(R.id.tvNumber);
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvDes = (TextView) v.findViewById(R.id.tvDes);
            imgCover = (ImageView) v.findViewById(R.id.imgCover);
            imgLocation = (ImageView) v.findViewById(R.id.imgLocation);
            relayLocation = (RelativeLayout) v.findViewById(R.id.relayLocation);
            linerBuyVoucher = (LinearLayout) v.findViewById(R.id.linerBuyVoucher);

        }

//        public TextView getTextView() {
//            return textView;
//        }
    }

    public MyVoucherAdapter(Context context, ArrayList<MyVoucherEntity> arrayList, OnclickUsevoucher onclickUsevoucher) {
        this.context = context;
        this.arrayList = arrayList;
        this.onclickUsevoucher = onclickUsevoucher;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_usevoucher, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final MyVoucherEntity curentPost = arrayList.get(position);


        //viewHolder.tvPoin.setVisibility(View.GONE);

        if (curentPost.isIsused() == false) {
            viewHolder.tvBuy.setText(context.getResources().getString(R.string.sudung));
            viewHolder.tvNumber.setText(context.getResources().getString(R.string.denngay) + " " + curentPost.getEndUsingTime());
        } else {
            viewHolder.tvBuy.setText(context.getResources().getString(R.string.dasudung));
            viewHolder.tvNumber.setText(context.getResources().getString(R.string.ngay) + " " + curentPost.getEndUsingTime());
            viewHolder.linerBuyVoucher.setBackground(context.getResources().getDrawable(R.drawable.btn_button_xam_dam5dp));
        }


        viewHolder.tvName.setText(curentPost.getNameShop() + " - " + curentPost.getNameVoucher());
        viewHolder.tvDes.setText(curentPost.getDesVoucher());

        if (curentPost.getCoverVoucher() != null & !curentPost.getCoverVoucher().equals("") & !curentPost.getCoverVoucher().isEmpty()) {
            Picasso.with(context)
                    .load(curentPost.getCoverVoucher())
                    .fit()
                    .centerInside()
                    .error(R.drawable.bg_shopdefault)
                    .into(viewHolder.imgCover);
        }

        viewHolder.relayLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickUsevoucher.onclickViewLocation(view, curentPost);
            }
        });

        viewHolder.imgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclickUsevoucher.onclickUseVoucher(view, curentPost,position);
            }
        });

        viewHolder.linerBuyVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curentPost.isIsused() == false)
                    onclickUsevoucher.onclickUseVoucher(view, curentPost,position);
            }
        });

        //  viewHolder.imgMonDat.setImageDrawable(curentPost.getCover());


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static String IntToStringNoDecimal(int i) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        formatter.applyPattern("#,###");
        return formatter.format(i);
    }


}
